$(function () {
	$(document).ready(function ($) {
		$('#success-subscribe').hide();
		$('#error-subscribe').hide();
		$('#subscribe-newsletter-form').submit(function (e) {
			e.preventDefault();
			e.stopPropagation();
			var email = $('#newsletter-input').val();
			$.ajax({
				beforeSend: function (xhrObj) {
					xhrObj.setRequestHeader("X-Requested-With", "XMLHttpRequest");
					xhrObj.setRequestHeader("Content-Type", "application/json");
					xhrObj.setRequestHeader("Accept", "application/json");
				},
				url: $(this).attr('action'),
				method: 'get',
				data: {email: email},
				success: function (response) {
					console.log(response);
					response = JSON.parse(response);
					if (response.success) {
						$('#newsletter-input').val('')
						$('#error-subscribe').hide();
						$('#success-subscribe').show();
						setTimeout(function () {
							$('#success-subscribe').hide();
						}, 3000)
					} else {
						$('#success-subscribe').hide();
						$('#error-subscribe').show();
						setTimeout(function () {
							$('#error-subscribe').hide();
						}, 3000)
					}
				},
			});
		});
	});
});
