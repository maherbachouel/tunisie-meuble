-- phpMyAdmin SQL Dump
-- version 4.8.5
-- https://www.phpmyadmin.net/
--
-- Hôte : 127.0.0.1:3306
-- Généré le :  mer. 19 fév. 2020 à 10:45
-- Version du serveur :  5.7.26
-- Version de PHP :  7.2.18

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de données :  `tunisie-meuble`
--

-- --------------------------------------------------------

--
-- Structure de la table `blog`
--

DROP TABLE IF EXISTS `blog`;
CREATE TABLE IF NOT EXISTS `blog` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(255) NOT NULL,
  `description` text NOT NULL,
  `banner` varchar(255) NOT NULL,
  `meta_description` varchar(255) NOT NULL,
  `permalink` varchar(255) NOT NULL,
  `status` int(11) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Structure de la table `category`
--

DROP TABLE IF EXISTS `category`;
CREATE TABLE IF NOT EXISTS `category` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `meta_description` varchar(255) NOT NULL,
  `permalink` varchar(255) NOT NULL,
  `icon` varchar(255) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=22 DEFAULT CHARSET=utf8;

--
-- Déchargement des données de la table `category`
--

INSERT INTO `category` (`id`, `name`, `meta_description`, `permalink`, `icon`, `created_at`, `updated_at`) VALUES
(3, 'Table de nuit', 'Tunisie meuble - Meuble ,Table de nuit à Tunis: service de vente, Tunisie meuble, Avenue Cité Sportive, La Soukra', 'table-de-nuit', 'f-icon f-icon-nightstand', '2020-01-13 11:37:21', '2020-02-19 09:57:52'),
(4, 'Garde-robe', 'Tunisie meuble - Garde-robe, Vente de  Garde-robe pas chère (+216) 24 005 160', 'garde-robe', 'f-icon f-icon-wardrobe', '2020-01-13 11:37:33', '2020-02-19 10:13:05'),
(5, 'Ensembles de barres', 'Tunisie meuble - Ensembles de barres,  retrouvez tous les meuble sur mesure et accessoires pour maison.', 'ensembles-de-barres', 'f-icon f-icon-bar-set', '2020-01-13 12:37:31', '2020-02-19 10:13:41'),
(6, 'Tables', 'Tables , meuble à prix pas cher en Tunisie, contactez nous (+216) 24 005 160 , (+216) 25 602 380', 'tables', 'f-icon f-icon-table', '2020-01-13 12:37:47', '2020-02-19 10:14:32'),
(7, 'Bibliothèque', 'Tunisie meuble : Découvrez notre gamme de meuble,Bibliothèque à Tunis', 'bibliotheque', 'f-icon f-icon-bookcase', '2020-01-13 12:38:02', '2020-02-19 10:15:07'),
(8, 'Cuisine', 'Tunisie meuble - accessoires et ustensiles de cuisine sur toute la Tunisie | tunisie-meuble.com :Articles de cuisine avec des prix pas cher', 'cuisine', 'f-icon f-icon-kitchen', '2020-01-13 12:38:18', '2020-02-19 10:15:57'),
(9, 'Tables à manger', 'Tunisie meuble - Découvrez le plus large choix de table à manger, avec une facilité de paiement et livraison sur toute la Tunisie', 'tables-a-manger', 'f-icon f-icon-dining-table', '2020-01-13 12:42:56', '2020-02-19 10:16:51'),
(12, 'Salon', 'Livraison sur toute la tunisie Meuble de salon moderne design et sur mesure chez  Tunisie meuble', 'salon', 'f-icon f-icon-sofa', '2020-01-14 07:58:24', '2020-02-19 10:17:23'),
(14, 'Fauteuils', 'Fauteuils ,Découvrez la gamme des chaises à bascule chez Tunisie meuble, Les chaises berçants sont maintenant disponible chez Tunisie-meuble.com', 'fauteuils', 'f-icon f-icon-armchair', '2020-02-19 09:35:22', '2020-02-19 10:18:09'),
(15, 'Chaises', 'Découvrez la gamme des chaises chez tunisie-meuble.com, Les chaises berçants sont maintenant disponible à soukra - tunisie', 'chaises', 'f-icon f-icon-chair', '2020-02-19 09:35:53', '2020-02-19 10:19:40'),
(16, 'Stockage média', 'Tunisie meuble  - Découvrez nos Stockage média moderne  large choix qui répond à vos besoins', 'stockage-media', 'f-icon f-icon-media-cabinet', '2020-02-19 09:36:21', '2020-02-19 10:20:21'),
(17, 'Chambre d\'enfants', 'Lit enfant, Armoire enfant, Bureau enfant, chaise enfant, différents choix de meubles enfant chez Tunisie meuble', 'chambre-denfants', 'f-icon f-icon-children-room', '2020-02-19 09:37:00', '2020-02-19 10:20:53'),
(19, 'Pack Mariage', 'Différent pack de mariage pack economique, pack silver, pack gold meubles disponible chez tunisie-meuble.com', 'pack-mariage', 'f-icon f-icon-children-room', '2020-02-19 09:38:13', '2020-02-19 10:24:56'),
(21, 'Bureau', 'Donnez une touche de décoration nordique à votre espace en choisissant le Bureau design qui vous convient. Notre magasin vous offre les meilleures styles avec des prix acceptables.', 'bureau', 'f-icon f-icon-office', '2020-02-19 09:40:53', '2020-02-19 10:24:35');

-- --------------------------------------------------------

--
-- Structure de la table `commande`
--

DROP TABLE IF EXISTS `commande`;
CREATE TABLE IF NOT EXISTS `commande` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) DEFAULT NULL,
  `firstname` varchar(255) NOT NULL,
  `lastname` varchar(255) NOT NULL,
  `email` varchar(255) NOT NULL,
  `phone` varchar(255) NOT NULL,
  `address` varchar(255) NOT NULL,
  `comment` text NOT NULL,
  `payment_method` varchar(255) NOT NULL,
  `product_id` int(11) DEFAULT NULL,
  `status` int(11) NOT NULL,
  `created_at` datetime NOT NULL,
  `order_price` float DEFAULT NULL,
  `color` varchar(255) DEFAULT NULL,
  `file` text,
  `advance` float DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `FK_product_id_order` (`product_id`)
) ENGINE=MyISAM AUTO_INCREMENT=19 DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Structure de la table `newsletter`
--

DROP TABLE IF EXISTS `newsletter`;
CREATE TABLE IF NOT EXISTS `newsletter` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `email` varchar(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Structure de la table `page`
--

DROP TABLE IF EXISTS `page`;
CREATE TABLE IF NOT EXISTS `page` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `slug` varchar(255) NOT NULL,
  `meta_title` varchar(255) NOT NULL,
  `meta_description` varchar(255) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8;

--
-- Déchargement des données de la table `page`
--

INSERT INTO `page` (`id`, `slug`, `meta_title`, `meta_description`, `created_at`, `updated_at`) VALUES
(1, 'accueil', 'Accueil - Tunisie meuble : meubles sur commande, chambre à coucher, salon pas chère (+216) 24 005 160', 'Tunisie meuble : meubles, chambre à coucher, salle à manger, salon, chambre enfants, tables et bibliothèque (+216) 24 005 160', '2020-02-19 09:44:14', '2020-02-19 09:49:05'),
(2, 'apropos', 'Tunisie meuble : Trouvez tous les informations nécessaires', 'Trouvez tous les informations nécessaires de Tunisie meuble : meubles, chambre à coucher, salle à manger, salon, chambre enfants, tables et bibliothèque', '2020-02-19 09:47:58', '2020-02-19 09:47:58'),
(3, 'produits', 'Tunisie meuble : Consultez les différents meuble offerts à Tunis.', 'Vente meuble à Tunis, Produits à vendre par Tunisie meuble: Consultez nos différents produits ... Service de vente. Prestation de qualité et Pro !', '2020-02-19 09:50:30', '2020-02-19 09:50:30'),
(4, 'commander', 'Tunisie meuble - Commander', 'Tunisie meuble - Commander', '2020-02-19 09:51:24', '2020-02-19 09:51:24'),
(5, 'blog', 'Trouvez les dernières actualités sur le meuble', 'Blog Tunisie meuble : Les nouvelles et les dernières actualités. Consultez le blog su Tunisie meuble.', '2020-02-19 09:54:48', '2020-02-19 09:54:48'),
(6, 'contact', 'Tunisie meuble : Coordonnées et les informations professionnelles (+216) 25 602 380', 'Trouvez tous les coordonnés et les différentes informations professionnelles de Tunisie meuble. Contactez-nous (+216) 25 602 380. ', '2020-02-19 09:55:34', '2020-02-19 09:55:34'),
(7, 'login', ' Se connecter à Tunisie Meuble', ' Se connecter à Tunisie Meuble', '2020-02-19 10:45:09', '2020-02-19 10:45:09');

-- --------------------------------------------------------

--
-- Structure de la table `product`
--

DROP TABLE IF EXISTS `product`;
CREATE TABLE IF NOT EXISTS `product` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `meta_description` varchar(255) NOT NULL,
  `permalink` varchar(255) NOT NULL,
  `price` float NOT NULL,
  `status` int(11) NOT NULL,
  `banner` text NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime DEFAULT NULL,
  `category_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `category_id` (`category_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Structure de la table `user`
--

DROP TABLE IF EXISTS `user`;
CREATE TABLE IF NOT EXISTS `user` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `email` varchar(255) NOT NULL,
  `firstname` varchar(255) NOT NULL,
  `lastname` varchar(255) NOT NULL,
  `username` varchar(255) NOT NULL,
  `api_key` varchar(255) NOT NULL,
  `password` varchar(255) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `role` varchar(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;

--
-- Déchargement des données de la table `user`
--

INSERT INTO `user` (`id`, `email`, `firstname`, `lastname`, `username`, `api_key`, `password`, `created_at`, `updated_at`, `role`) VALUES
(4, 'contact@tunisie-meuble.com', 'tunisie', 'meuble', 'tunisie-meuble', 'qKjp4IqCf5UV8DpVPUq1xRCEVm5U1MNGVe99M8DFmbgRe9IEVmFKfIpJeDtVe5YqKjpPUq41MNG1xRC9MFmbgRJeDtqCfIFKfe5Y', 'a21d17b9ec385fea662d3a6a041b1600f749e8c34b2520ce5be9ca51f46eb5974d2f3663520a2aed993595703e8d924df9763309f334c42ad9e1356a32b1b742i7XlKofeTrOolX6HKps7mTtAKRY0YkT3PPF1jdJFI/Q=', '2018-12-28 15:07:56', '2020-02-10 11:06:30', 'SUPER_ADMIN');

--
-- Contraintes pour les tables déchargées
--

--
-- Contraintes pour la table `product`
--
ALTER TABLE `product`
  ADD CONSTRAINT `category_id` FOREIGN KEY (`category_id`) REFERENCES `category` (`id`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
