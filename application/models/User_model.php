<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class User_model extends MY_Model
{
	protected $table = 'user';
	public function AuthenficationUser($email)
	{
		return $this->db->select('*')
			->from($this->table)
			->where('email', $email)
			->get()
			->result();
	}

	public function GetUserById($id)
	{
		return $this->db->select('*')
			->from($this->table)
			->where($this->table . '.id', $id)
			->get()
			->result();
	}
}
