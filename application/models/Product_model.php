<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Product_model extends MY_Model
{
	protected $table = 'product';

	public function getAllProducts($where, $nb = null, $debut = null)
	{
		$this->db->select('product.*,
		category.permalink as category_permalink,
		category.meta_description as category_meta_description,
		category.created_at as category_created_at,
		category.updated_at as category_updated_at,
		category.name as category_name,
		category.id as category_id')
			->from($this->table)
			->join('category', 'category.id=product.category_id', 'left')
			->where($where);
		if ($nb != null && $debut != null) {
			$this->db->limit($nb, $debut);
		};
		return $this->db->get()
			->result();
	}
}
