<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Newsletter_model extends MY_Model {

    protected $table = 'newsletter';

    public function DeleteNewsletter($id) {
        $this->db->where(['id' => $id])->delete('newsletter');
    }
    public function createNewsletter($options=array(),$functions=array()){
        if (empty($options) && empty($functions)) {
            return false;
        }
        return (bool) $this->db->set($options)->set($functions,null,false)->insert($this->table);
    }
    public function readNewsletter($select = '*', $where = array(), $nb = null, $debut = null){
        return $this->db->select($select)
            ->from($this->table)
            ->where($where)
            ->limit($nb, $debut)
            ->get()
            ->result();
    }
}