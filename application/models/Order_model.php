<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Order_model extends MY_Model
{
	protected $table = 'commande';

	public function getAllOrders($where = array())
	{
		return $this->db->select('commande.*,
		product.id as product_id,
		product.banner as product_file,
		product.name as product_name,
		product.created_at as product_created_at')
			->from($this->table)
			->join('product', 'product.id=commande.product_id', 'left')
			->where($where)
			->get()
			->result();
	}


}
