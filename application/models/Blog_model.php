<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Blog_Model extends MY_Model
{
    protected $table = 'blog';


    public function AddBlog($data)
    {
        return $this->db->insert($this->table, $data);
    }

    public function getBlogById($id)
    {
        return $this->db->select('*')
            ->from($this->table)
            ->where('id', $id)
            ->get()
            ->result();
    }

    public function EditBlog($data, $id)
    {
        return $this->db->where('id', $id)
            ->update($this->table, $data);
    }

    public function getAllArticle()
    {
        return $this->db->select('*')
            ->from($this->table)
            ->where(['status' => 1])
            ->order_by($this->table . '.created_at', 'desc')
            ->get()
            ->result();
    }

    public function getRecentArticle()
    {
        return $this->db->select('*')
            ->from($this->table)
            ->where(['status' => 1])
            ->order_by($this->table . '.created_at', 'desc')
            ->limit(3, 0)
            ->get()
            ->result();
    }

    public function getArticleByPermalink($permalink)
    {
        return $this->db->select('*')
            ->from($this->table)
            ->where(['status' => 1])
            ->where('permalink', $permalink)
            ->get()
            ->result();
    }
}