<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Page_Model extends MY_Model {
    protected $table = 'page';


    public function AddPage($data) {
        return $this->db->insert($this->table, $data);
    }

    public function getPageById($id) {
        return $this->db->select('*')
            ->from($this->table)
            ->where('id', $id)
            ->get()
            ->result();
    }

    public function EditPage($data, $id) {
        return $this->db->where('id', $id)
            ->update($this->table, $data);
    }

    public function getMetaByPage($page) {
        return $this->db->select('*')
            ->from($this->table)
            ->where('slug', $page)
            ->get()
            ->result();
    }
}