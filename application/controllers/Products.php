<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Products extends MY_Controller
{

	function __construct()
	{
		parent::__construct();
		$this->load->model('category_model', 'category');
		$this->load->model('product_model', 'product');

	}

	public function index()
	{
		$data = array(
			"category" => $this->category->read('*'),
			"products" => $this->product->getAllProducts(array('status' => '1')),
		);
		$view = $this->load->view('common/header', $data, TRUE);
		$view .= $this->load->view('product/product', $data, TRUE);
		$view .= $this->load->view('common/footer', $data, TRUE);
		echo $view;
	}

	public function singleProduct($permalinkCat, $permalinkProduct)
	{
		$category = $this->category->read('*', array('permalink' => $permalinkCat));
		$product = $this->product->read('*', array('permalink' => $permalinkProduct));
		if (!count($category) || !count($product)) {
			redirect(base_url() . 'error_404');
		}
		$data = array(
			'product' => $this->product->getAllProducts(array('product.permalink' => $permalinkProduct, 'product.status' => '1'))[0],
			'ogImage' => base_url() . $product[0]->banner,
		);

		$data['meta']['meta_title'] = $product[0]->name;
		$data['meta']['meta_description'] = $product[0]->meta_description;
		$view = $this->load->view('common/header', $data, TRUE);
		$view .= $this->load->view('product/single', $data, TRUE);
		$view .= $this->load->view('common/footer', $data, TRUE);
		echo $view;
	}

	public function category($permalink)
	{
		$category = $this->category->read('*', array('permalink' => $permalink));
		if (!count($category)) {
			redirect(base_url() . 'error_404');
		}
		$data = array(
			"products" => $this->product->getAllProducts(array('category_id' => $category[0]->id)),
			"currentCategory" => $category[0],
			"category" => $this->category->read('*'),
		);
		$data['meta']['meta_title'] = $category[0]->name;
		$data['meta']['meta_description'] = $category[0]->meta_description;
		$view = $this->load->view('common/header', $data, TRUE);
		$view .= $this->load->view('product/category', $data, TRUE);
		$view .= $this->load->view('common/footer', $data, TRUE);
		echo $view;
	}
}
