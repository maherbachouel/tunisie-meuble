<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Order extends MY_Controller
{

	function __construct()
	{
		parent::__construct();
		$this->load->model('product_model', 'product');
		$this->load->model('order_model', 'order');
	}

	public function index($id)
	{
		$product = $this->product->read('*', array('id' => $id));
		if (!count($product)) {
			redirect(base_url() . 'error_404');
		}
		if ($_SERVER['REQUEST_METHOD'] == "POST") {
			$product = $this->product->read('*', array('id' => $id));
			$product = $product[0];
			$order = array(
				'created_at' => date('Y-m-d H:i:s'),
				'firstname' => $this->input->post('firstname'),
				'lastname' => $this->input->post('lastname'),
				'address' => $this->input->post('address'),
				'email' => $this->input->post('email'),
				'phone' => $this->input->post('phone'),
				'comment' => $this->input->post('comment'),
				'payment_method' => $this->input->post('paymentOption'),
				'product_id' => $id,
				'order_price' => $product->price,
				'status' => 0,
				'advance' => 0
			);
			$this->order->add($order);
			redirect(base_url() . 'validate-order');
		}
		$data = array(
			'product' => $product[0]
		);
		$view = $this->load->view('common/header', $data, TRUE);
		$view .= $this->load->view('order/product_order', $data, TRUE);
		$view .= $this->load->view('common/footer', $data, TRUE);
		echo $view;
	}

	public function simple()
	{
		if ($_SERVER['REQUEST_METHOD'] == "POST") {
			$order = array(
				'created_at' => date('Y-m-d H:i:s'),
				'firstname' => $this->input->post('firstname'),
				'lastname' => $this->input->post('lastname'),
				'address' => $this->input->post('address'),
				'email' => $this->input->post('email'),
				'phone' => $this->input->post('phone'),
				'comment' => $this->input->post('comment'),
				'payment_method' => $this->input->post('paymentOption'),
				'status' => 0,
				'advance' => 0
			);
			if ($_FILES['image'] && isset($_FILES['image'])) {
				$config['upload_path'] = './uploads/order/';
				$config['allowed_types'] = 'gif|jpg|png|jpeg';
				$this->load->library('upload', $config);
				if ($this->upload->do_upload('image')) {
					$this->upload->data();
					$order['file'] = 'uploads/order/' . $this->upload->data('file_name');
				} else {
					echo $this->upload->display_errors();
				}
			}
			$this->order->add($order);
			redirect(base_url() . 'validate-order');
		}
		$data = array();
		$view = $this->load->view('common/header', $data, TRUE);
		$view .= $this->load->view('order/simple_order', $data, TRUE);
		$view .= $this->load->view('common/footer', $data, TRUE);
		echo $view;
	}

	public function validate_order()
	{
		$data = array();
		$view = $this->load->view('common/header', $data, TRUE);
		$view .= $this->load->view('order/validate_order', $data, TRUE);
		$view .= $this->load->view('common/footer', $data, TRUE);
		echo $view;
	}
}
