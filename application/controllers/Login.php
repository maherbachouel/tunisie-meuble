<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Login extends MY_Controller
{

	function __construct()
	{
		parent::__construct();
		$this->load->model('user_model', 'account');
	}

	public function index()
	{
		if ($this->session->userdata('uid')) {
			redirect(base_url() . 'admin/dashboard');
		}
		if ($_SERVER['REQUEST_METHOD'] == "POST") {
			$email = $this->input->post('email');
			$password = $this->input->post('password');
			$user = $this->account->AuthenficationUser($email);
			if (count($user) == 1) {
				if ($this->encryption->decrypt($user[0]->password) === $password) {
					$uid = $user[0]->id;
					$role = $user[0]->profile_type;
					$this->session->set_userdata('loggedIn', '1');
					$this->session->set_userdata('uid', $uid);
					$this->session->set_userdata('role', $role);
					$expire = time() + (86400 * 30);
					setcookie('uid', $this->encryption->encrypt($uid), $expire, "/", $this->config->item('domain'), 0);
					redirect(base_url() . 'admin/dashboard');
				}
			}
		}
		$data = array();
		$view = $this->load->view('common/header', $data, TRUE);
		$view .= $this->load->view('login', $data, TRUE);
		$view .= $this->load->view('common/footer', $data, TRUE);
		echo $view;
	}

	public function logout()
	{
		$this->session->sess_destroy();
		redirect(base_url());
	}

}
