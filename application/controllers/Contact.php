<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Contact extends MY_Controller
{

	function __construct()
	{
		parent::__construct();

	}

	public function index()
	{
		if ($_SERVER['REQUEST_METHOD'] == "POST") {
			$success = false;
			$data = array(
				'name' => $this->input->post('name'),
				'email' => $this->input->post('email'),
				'phone' => $this->input->post('phone'),
				'subject' => $this->input->post('subject'),
				'message' => $this->input->post('message')
			);
			$config = $this->config->item('email');
			$this->email->initialize($config);
			$this->email->set_newline("\r\n");
			$this->email->from($config['smtp_user']);
			$this->email->to($config['smtp_user']);
			$this->email->subject($data['subject']);
			$message = 'Nom et prénom: ' . $data['name'] . "<br><br>" . 'Téléphone: ' . $data['phone'] . "<br><br>" . 'Email: ' . $data['email'] . "<br><br>" . 'Sujet: ' . stripslashes($data['subject']) . "<br><br>" . 'Message: ' . stripslashes($data['message']);
			$this->email->message($message);
			if ($this->email->send()) {
				$success = true;
			}
			$this->session->set_flashdata('success', $success);
			redirect(base_url() . 'contact');
		}
		$data = array();
		$view = $this->load->view('common/header', $data, TRUE);
		$view .= $this->load->view('contact', $data, TRUE);
		$view .= $this->load->view('common/footer', $data, TRUE);
		echo $view;
	}


}
