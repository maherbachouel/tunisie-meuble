<?php
defined('BASEPATH') OR exit('No direct script access allowed');
include_once(APPPATH . 'core/Admin_Controller.php');

class Profile extends Admin_Controller
{
	function __construct()
	{
		parent::__construct();
		$this->load->model('user_model', 'user');
	}

	public function index()
	{

		if ($_SERVER['REQUEST_METHOD'] == "POST") {
			$user = array(
				'updated_at' => date('Y-m-d H:i:s'),
				'email' => $this->input->post('email'),
				'firstname' => $this->input->post('firstname'),
				'lastname' => $this->input->post('lastname'),
				'password' => $this->encryption->encrypt($this->input->post('password'))
			);
			$this->user->update(array('id' => $this->current_user->id), $user);
			redirect(base_url() . 'admin/profile');
		}
		$data = array(
			"activepage" => "profile",
			"user" => $this->current_user,
		);

		$view = $this->load->view('admin/common/header', $data, TRUE);
		$view .= $this->load->view('admin/profile', $data, TRUE);
		$view .= $this->load->view('admin/common/footer', $data, TRUE);
		echo $view;
	}
}
