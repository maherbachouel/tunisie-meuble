<?php
defined('BASEPATH') OR exit('No direct script access allowed');
include_once(APPPATH . 'core/Admin_Controller.php');

class Product extends Admin_Controller
{

	function __construct()
	{
		parent::__construct();
		$this->load->model('product_model', 'product');
		$this->load->model('category_model', 'category');
	}

	public function index()
	{
		$data = array(
			"activepage" => "product",
			"products" => $this->product->read('*'),
		);
		$view = $this->load->view('admin/common/header', $data, TRUE);
		$view .= $this->load->view('admin/product/list', $data, TRUE);
		$view .= $this->load->view('admin/common/footer', $data, TRUE);
		echo $view;
	}

	public function add()
	{
		if ($_SERVER['REQUEST_METHOD'] == "POST") {
			$product = array(
				'created_at' => date('Y-m-d H:i:s'),
				'permalink' => $this->input->post('permalink'),
				"meta_description" => $this->input->post('meta_description'),
				"category_id" => $this->input->post('category_id'),
				"price" => $this->input->post('price'),
				"status" => $this->input->post('status'),
				"name" => $this->input->post('name'),
			);
			if ($_FILES['image'] && isset($_FILES['image'])) {
				$config['upload_path'] = './uploads/product/';
				$config['allowed_types'] = 'gif|jpg|png|jpeg';
				$this->load->library('upload', $config);
				if ($this->upload->do_upload('image')) {
					$this->upload->data();
					$product['banner'] = 'uploads/product/' . $this->upload->data('file_name');
				} else {
					echo $this->upload->display_errors();
				}
			}
			$this->product->add($product);
			redirect(base_url() . 'admin/product');
		}
		$data = array(
			"activepage" => "product",
			"category" => $this->category->read('*'),
		);
		$view = $this->load->view('admin/common/header', $data, TRUE);
		$view .= $this->load->view('admin/product/add', $data, TRUE);
		$view .= $this->load->view('admin/common/footer', $data, TRUE);
		echo $view;
	}

	public function edit($id)
	{
		$nbr = $this->product->read('*', array('id' => $id));
		if (!$id || !count($nbr)) {
			redirect(base_url() . 'admin/product');
		}
		if ($_SERVER['REQUEST_METHOD'] == "POST") {
			$product = array(
				'created_at' => date('Y-m-d H:i:s'),
				'permalink' => $this->input->post('permalink'),
				"meta_description" => $this->input->post('meta_description'),
				"category_id" => $this->input->post('category_id'),
				"price" => $this->input->post('price'),
				"status" => $this->input->post('status'),
				"name" => $this->input->post('name'),
			);
			if ($_FILES['image'] && isset($_FILES['image'])) {
				$config['upload_path'] = './uploads/product/';
				$config['allowed_types'] = 'gif|jpg|png|jpeg';
				$this->load->library('upload', $config);
				if ($this->upload->do_upload('image')) {
					$this->upload->data();
					$product['banner'] = 'uploads/product/' . $this->upload->data('file_name');
				} else {
					echo $this->upload->display_errors();
				}
			}
			$this->product->update(array('id' => $id), $product);
			redirect(base_url() . 'admin/product');
		}
		$data = array(
			"activepage" => "product",
			"product" => $this->product->read('*', array('id' => $id))[0],
			"category" => $this->category->read('*'),
		);
		$view = $this->load->view('admin/common/header', $data, TRUE);
		$view .= $this->load->view('admin/product/edit', $data, TRUE);
		$view .= $this->load->view('admin/common/footer', $data, TRUE);
		echo $view;
	}

	public function delete($id)
	{

		$this->product->delete(array('id' => $id));
		redirect(base_url() . 'admin/product');
	}


}
