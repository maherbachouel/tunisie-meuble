<?php
defined('BASEPATH') OR exit('No direct script access allowed');
include_once(APPPATH . 'core/Admin_Controller.php');

class Category extends Admin_Controller
{

	function __construct()
	{
		parent::__construct();
		$this->load->model('category_model', 'category');
	}

	public function index()
	{
		$data = array(
			"activepage" => "category",
			"category" => $this->category->read('*'),
		);
		$view = $this->load->view('admin/common/header', $data, TRUE);
		$view .= $this->load->view('admin/category/list', $data, TRUE);
		$view .= $this->load->view('admin/common/footer', $data, TRUE);
		echo $view;
	}

	public function add()
	{
		if ($_SERVER['REQUEST_METHOD'] == "POST") {
			$category = array(
				'created_at' => date('Y-m-d H:i:s'),
				'permalink' => $this->input->post('permalink'),
				"meta_description" => $this->input->post('meta_description'),
				"icon" => $this->input->post('icon'),
				"name" => $this->input->post('name'),
			);
			$this->category->add($category);
			redirect(base_url() . 'admin/category');
		}
		$data = array(
			"activepage" => "category",
		);
		$view = $this->load->view('admin/common/header', $data, TRUE);
		$view .= $this->load->view('admin/category/add', $data, TRUE);
		$view .= $this->load->view('admin/common/footer', $data, TRUE);
		echo $view;
	}

	public function edit($id)
	{
		$nbr = $this->category->read('*', array('id' => $id));
		if (!$id || !count($nbr)) {
			redirect(base_url() . 'admin/category');
		}
		if ($_SERVER['REQUEST_METHOD'] == "POST") {
			$category = array(
				'updated_at' => date('Y-m-d H:i:s'),
				'permalink' => $this->input->post('permalink'),
				"meta_description" => $this->input->post('meta_description'),
				"icon" => $this->input->post('icon'),
				"name" => $this->input->post('name'),
			);
			$this->category->update(array('id' => $id), $category);
			redirect(base_url() . 'admin/category');
		}
		$data = array(
			"activepage" => "category",
			"category" => $this->category->read('*', array('id' => $id))[0],
		);
		$view = $this->load->view('admin/common/header', $data, TRUE);
		$view .= $this->load->view('admin/category/edit', $data, TRUE);
		$view .= $this->load->view('admin/common/footer', $data, TRUE);
		echo $view;
	}

	public function delete($id)
	{

		$this->category->delete(array('id' => $id));
		redirect(base_url() . 'admin/category');
	}


}
