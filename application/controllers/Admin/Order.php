<?php
defined('BASEPATH') OR exit('No direct script access allowed');
include_once(APPPATH . 'core/Admin_Controller.php');

class Order extends Admin_Controller
{

	function __construct()
	{
		parent::__construct();
		$this->load->model('order_model', 'order');
	}

	public function index()
	{
		$data = array(
			"activepage" => "order",
			"orders" => $this->order->getAllOrders(array()),
		);
		$view = $this->load->view('admin/common/header', $data, TRUE);
		$view .= $this->load->view('admin/order/list', $data, TRUE);
		$view .= $this->load->view('admin/common/footer', $data, TRUE);
		echo $view;
	}

	public function add()
	{
		if ($_SERVER['REQUEST_METHOD'] == "POST") {
			$order = array(
				'created_at' => date('Y-m-d H:i:s'),
				'name' => $this->input->post('name'),
				'firstname' => $this->input->post('firstname'),
				'lastname' => $this->input->post('lastname'),
				'address' => $this->input->post('address'),
				'email' => $this->input->post('email'),
				'phone' => $this->input->post('phone'),
				'order_price' => $this->input->post('order_price'),
				'comment' => $this->input->post('comment'),
				'payment_method' => $this->input->post('payment_method'),
				'status' => $this->input->post('status'),
				'advance' => $this->input->post('advance'),
				'color' => $this->input->post('color')
			);
			if ($_FILES['image'] && isset($_FILES['image'])) {
				$config['upload_path'] = './uploads/order/';
				$config['allowed_types'] = 'gif|jpg|png|jpeg';
				$this->load->library('upload', $config);
				if ($this->upload->do_upload('image')) {
					$this->upload->data();
					$order['file'] = 'uploads/order/' . $this->upload->data('file_name');
				} else {
					echo $this->upload->display_errors();
				}
			}
			$this->order->add($order);
			redirect(base_url() . 'admin/order');
		}
		$data = array(
			"activepage" => "order",
		);
		$view = $this->load->view('admin/common/header', $data, TRUE);
		$view .= $this->load->view('admin/order/add', $data, TRUE);
		$view .= $this->load->view('admin/common/footer', $data, TRUE);
		echo $view;
	}

	public function edit($id)
	{
		$nbr = $this->order->read('*', array('id' => $id));
		if (!$id || !count($nbr)) {
			redirect(base_url() . 'admin/order');
		}
		if ($_SERVER['REQUEST_METHOD'] == "POST") {
			$order = array(
				'name' => $this->input->post('name'),
				'firstname' => $this->input->post('firstname'),
				'lastname' => $this->input->post('lastname'),
				'address' => $this->input->post('address'),
				'email' => $this->input->post('email'),
				'phone' => $this->input->post('phone'),
				'order_price' => $this->input->post('order_price'),
				'comment' => $this->input->post('comment'),
				'payment_method' => $this->input->post('payment_method'),
				'status' => $this->input->post('status'),
				'advance' => $this->input->post('advance'),
				'color' => $this->input->post('color')
			);
			if ($_FILES['image'] && isset($_FILES['image'])) {
				$config['upload_path'] = './uploads/order/';
				$config['allowed_types'] = 'gif|jpg|png|jpeg';
				$this->load->library('upload', $config);
				if ($this->upload->do_upload('image')) {
					$this->upload->data();
					$order['file'] = 'uploads/order/' . $this->upload->data('file_name');
				} else {
					echo $this->upload->display_errors();
				}
			}
			$this->order->update(array('id' => $id), $order);
			redirect(base_url() . 'admin/order');
		}
		$data = array(
			"activepage" => "order",
			"order" => $this->order->getAllOrders(array('commande.id' => $id))[0],
		);
		$view = $this->load->view('admin/common/header', $data, TRUE);
		$view .= $this->load->view('admin/order/edit', $data, TRUE);
		$view .= $this->load->view('admin/common/footer', $data, TRUE);
		echo $view;
	}

	public function delete($id)
	{

		$this->order->delete(array('id' => $id));
		redirect(base_url() . 'admin/order');
	}


}
