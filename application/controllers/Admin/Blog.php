<?php
defined('BASEPATH') OR exit('No direct script access allowed');

include_once(APPPATH . 'core/Admin_Controller.php');

class Blog extends Admin_Controller
{

    function __construct()
    {
        parent::__construct();
        $this->load->model('blog_model', 'blog');
    }


    public function index()
    {
        $data = array(
            "user" => $this->current_user,
            "activepage" => "blog",
            'articles' => $this->blog->read('*'),
            'published' => $this->blog->count(array("status" => 1)),
            'draft' => $this->blog->count(array("status" => 0))
        );
        $view = $this->load->view('admin/common/header', $data, TRUE);
        $view .= $this->load->view('admin/blog/liste', $data, TRUE);
        $view .= $this->load->view('admin/common/footer', $data, TRUE);
        echo $view;
    }

    public function add()
    {
        if ($_SERVER['REQUEST_METHOD'] == "POST") {

            $data = array(
                "title" => $this->input->post('title'),
                "description" => $this->input->post('description'),
                "status" => $this->input->post('status'),
                "meta_description" => $this->input->post('meta_description'),
                "permalink" => $this->input->post('permalink'),
                "created_at" => date('Y-m-d H:i:s'),
                "updated_at" => date('Y-m-d H:i:s')
            );
            if (isset($_FILES['banner']['name']) && $_FILES['banner']['name']) {
                $upload_dir = FCPATH . 'uploads/';
                $formations_dir = $upload_dir . 'blog/';
                $image_dir = 'uploads/blog/';
                if (!is_dir($upload_dir)) {
                    mkdir($upload_dir);
                }
                if (!is_dir($formations_dir)) {
                    mkdir($formations_dir);
                }
                $filename = time();
                $extension = strtolower(pathinfo($_FILES['banner']['name'], PATHINFO_EXTENSION));
                $image = $filename . '.' . $extension;

                if (!move_uploaded_file($_FILES['banner']['tmp_name'], $formations_dir . $image)) {
                    $errors[] = 'ATTACHMENT_FILE_NOT_VALID';
                    $image = null;
                } else {
                    $data['banner'] = $image_dir . $image;
                }
            }

            $this->blog->AddBlog($data);
            redirect(base_url() . 'admin/blog');
        }
        $data = array(
            "user" => $this->current_user,
            "activepage" => "blog",
        );
        $view = $this->load->view('admin/common/header', $data, TRUE);
        $view .= $this->load->view('admin/blog/add', $data, TRUE);
        $view .= $this->load->view('admin/common/footer', $data, TRUE);
        echo $view;
    }

    public function edit($id)
    {

        if ($_SERVER['REQUEST_METHOD'] == "POST") {
            $data = array(
                "title" => $this->input->post('title'),
                "description" => $this->input->post('description'),
                "status" => $this->input->post('status'),
                "meta_description" => $this->input->post('meta_description'),
                "permalink" => $this->input->post('permalink'),
                "updated_at" => date('Y-m-d H:i:s')
            );
            if (isset($_FILES['banner']['name']) && $_FILES['banner']['name']) {
                $upload_dir = FCPATH . 'uploads/';
                $formations_dir = $upload_dir . 'blog/';
                $image_dir = 'uploads/blog/';
                if (!is_dir($upload_dir)) {
                    mkdir($upload_dir);
                }
                if (!is_dir($formations_dir)) {
                    mkdir($formations_dir);
                }
                $filename = time();
                $extension = strtolower(pathinfo($_FILES['banner']['name'], PATHINFO_EXTENSION));
                $image = $filename . '.' . $extension;

                if (!move_uploaded_file($_FILES['banner']['tmp_name'], $formations_dir . $image)) {
                    $errors[] = 'ATTACHMENT_FILE_NOT_VALID';
                    $image = null;
                } else {
                    $data['banner'] = $image_dir . $image;
                }
            }
            $this->blog->EditBlog($data, $id);
            redirect(base_url() . 'admin/blog');
        }
        if (!$this->blog->getBlogById($id)) {
            redirect(base_url() . 'admin/blog');
        }
        $data = array(
            "user" => $this->current_user,
            "article" => $this->blog->getBlogById($id)[0],
            "activepage" => 'blog'
        );
        $view = $this->load->view('admin/common/header', $data, TRUE);
        $view .= $this->load->view('admin/blog/edit', $data, TRUE);
        $view .= $this->load->view('admin/common/footer', $data, TRUE);
        echo $view;
    }

    public function delete($id)
    {
        $this->blog->delete(array('id' => $id));
        redirect(base_url() . 'admin/blog');
    }
}
