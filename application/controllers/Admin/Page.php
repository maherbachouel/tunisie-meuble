<?php
defined('BASEPATH') OR exit('No direct script access allowed');

include_once(APPPATH . 'core/Admin_Controller.php');

class Page extends Admin_Controller
{

    function __construct()
    {
        parent::__construct();
        $this->load->model('page_model', 'page');
    }


    public function index()
    {
        $data = array(
            "user" => $this->current_user,
            "activepage" => "page",
            'pages' => $this->page->read('*')
        );
        $view = $this->load->view('admin/common/header', $data, TRUE);
        $view .= $this->load->view('admin/page/liste', $data, TRUE);
        $view .= $this->load->view('admin/common/footer', $data, TRUE);
        echo $view;
    }

    public function add()
    {
        if ($_SERVER['REQUEST_METHOD'] == "POST") {

            $data = array(
                "meta_title" => $this->input->post('title'),
                "meta_description" => $this->input->post('meta_description'),
                "slug" => $this->input->post('slug'),
                "created_at" => date('Y-m-d H:i:s'),
                "updated_at" => date('Y-m-d H:i:s')
            );
            $this->page->AddPage($data);
            redirect(base_url() . 'admin/page');
        }
        $data = array(
            "user" => $this->current_user,
            "activepage" => "page",
        );
        $view = $this->load->view('admin/common/header', $data, TRUE);
        $view .= $this->load->view('admin/page/add', $data, TRUE);
        $view .= $this->load->view('admin/common/footer', $data, TRUE);
        echo $view;
    }
    public function edit($id){

        if ($_SERVER['REQUEST_METHOD'] == "POST") {
            $data = array(
                "meta_title" => $this->input->post('title'),
                "meta_description" => $this->input->post('meta_description'),
                "slug" => $this->input->post('slug'),
                "updated_at" => date('Y-m-d H:i:s')
            );
            $this->page->EditPage($data, $id);
            redirect(base_url() . 'admin/page');
        }
        if (!$this->page->getPageById($id)) {
            redirect(base_url() . 'admin/page');
        }

        $data = array(
            "user" => $this->current_user,
            "page" => $this->page->getPageById($id)[0],
            "activepage" => 'page'
        );
        $view = $this->load->view('admin/common/header', $data, TRUE);
        $view .= $this->load->view('admin/page/edit', $data, TRUE);
        $view .= $this->load->view('admin/common/footer', $data, TRUE);
        echo $view;
    }
    public function delete($id)
    {
        $this->page->delete(array('id' => $id));
        redirect(base_url() . 'admin/page');
    }
}
