<?php
defined('BASEPATH') OR exit('No direct script access allowed');
include_once(APPPATH . 'core/Admin_Controller.php');
class Dashboard extends Admin_Controller
{

	function __construct()
	{
		parent::__construct();
		$this->load->model('product_model', 'product');
		$this->load->model('order_model', 'order');

	}

	public function index()
	{
		$data = array(
			"activepage" => "dashboard",
			"orders" => $this->order->count(),
			"products" =>  $this->product->count(),
		);
		$view = $this->load->view('admin/common/header', $data, TRUE);
		$view .= $this->load->view('admin/dashboard', $data, TRUE);
		$view .= $this->load->view('admin/common/footer', $data, TRUE);
		echo $view;
	}


}
