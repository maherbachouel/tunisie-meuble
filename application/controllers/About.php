<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class About extends MY_Controller
{

	function __construct()
	{
		parent::__construct();

	}

	public function index()
	{

		$data = array();
		$view = $this->load->view('common/header', $data, TRUE);
		$view .= $this->load->view('about', $data, TRUE);
		$view .= $this->load->view('common/footer', $data, TRUE);
		echo $view;
	}


}
