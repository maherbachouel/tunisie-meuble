<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Blog extends MY_Controller
{
	public function __construct()
	{
		parent::__construct();
		$this->load->model('blog_model', 'blog');
		$this->load->model('category_model', 'category');
	}

	public function index()
	{
		$data = array(
			'articles' => $this->blog->getAllArticle(),
			'categories' => $this->category->read('*'),
		);
		$view = $this->load->view('common/header', $data, TRUE);
		$view .= $this->load->view('blog/blog', $data, TRUE);
		$view .= $this->load->view('common/footer', $data, TRUE);
		echo $view;
	}

	public function singleBlog($permalink)
	{

		if (!$this->blog->getArticleByPermalink($permalink)[0]) {
			redirect(base_url() . 'erreur-404');
		}
		$data = array(
			'activePage' => 'blog',
			'filter' => '',
			'categories' => $this->category->read('*'),
			'article' => $this->blog->getArticleByPermalink($permalink)[0],
			'ogImage' => base_url() . $this->blog->getArticleByPermalink($permalink)[0]->banner,
			'meta' => array(
				'meta_title' => $this->blog->getArticleByPermalink($permalink)[0]->title,
				'meta_description' => $this->blog->getArticleByPermalink($permalink)[0]->meta_description
			)
		);
		$view = $this->load->view('common/header', $data, TRUE);
		$view .= $this->load->view('blog/single', $data, TRUE);
		$view .= $this->load->view('common/footer', $data, TRUE);
		echo $view;
	}
}
