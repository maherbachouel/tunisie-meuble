<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Error_404 extends MY_Controller
{

	public function __construct()
	{
		parent::__construct();
	}

	public function index()
	{
		$data = array();
		$view = $this->load->view('common/header', $data, TRUE);
		$view .= $this->load->view('errors/404', $data, TRUE);
		$view .= $this->load->view('common/footer', $data, TRUE);
		echo $view;
	}

}
