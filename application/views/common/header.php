<!DOCTYPE html>
<?php $CI =& get_instance(); ?>
<?php
error_reporting(0);
$ci =& get_instance();
$page_slug = $ci->uri->segment(1);
$ci->load->model('page_model', 'page');
if ($page_slug === "services" && count($ci->uri->segment(2))) {
	$page_slug = $ci->uri->segment(1) . '/' . $ci->uri->segment(2);
	$meta = $ci->page->getMetaByPage($page_slug);
	if (!$meta) {
		$meta = array(
			'meta_title' => 'Tunisie Meuble',
			'meta_description' => 'Tunisie Meuble',
		);
	} else {
		$meta = array(
			'meta_title' => $meta[0]->meta_title,
			'meta_description' => $meta[0]->meta_description
		);
	}
} else {
	if (($page_slug || $page_slug == null) && !count($ci->uri->segment(2))) {

		if ($page_slug === null) {
			$page_slug = 'accueil';
		}
		$meta = $ci->page->getMetaByPage($page_slug);
		if (!$meta) {
			$meta = array(
				'meta_title' => 'Tunisie Meuble',
				'meta_description' => 'Tunisie Meuble',
			);
		} else {
			$meta = array(
				'meta_title' => $meta[0]->meta_title,
				'meta_description' => $meta[0]->meta_description
			);
		}
	}
}
$current_url = $ci->uri->segment(1);
?>
<html lang="fr">
<head>
	<meta charset="utf-8">
	<title><?php echo $meta['meta_title']; ?></title>
	<meta name="description" content="<?php echo $meta['meta_description']; ?>">
	<meta name="robots" content="index, follow">
	<meta name="theme-color" content="#bd0926" style="color: #bd0926;">
	<meta name="author" content="Maher Bachouel (maherbachouel@gmail.com)">
	<meta http-equiv="content-type" content="text/html;charset=UTF-8"/>
	<meta http-equiv="X-UA-Compatible" content="IE=11"/>
	<meta http-equiv="x-ua-compatible" content="ie=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<!-- Google / Search Engine Tags -->
	<meta itemprop="name" content="<?php echo $meta['meta_title']; ?>">
	<meta itemprop="description" content="<?php echo $meta['meta_description']; ?>">
	<meta itemprop="image" content="<?php echo isset($ogImage) ? $ogImage : base_url() . 'partage.jpg' ?>">
	<!-- Facebook Meta Tags -->
	<meta property="og:url" content="<?php echo "https://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]"; ?>">
	<meta property="og:type" content="website">
	<meta property="og:title" content="<?php echo $meta['meta_title']; ?>">
	<meta property="og:description" content="<?php echo $meta['meta_description']; ?>">
	<meta property="og:image" content="<?php echo isset($ogImage) ? $ogImage : base_url() . 'partage.jpg' ?>">
	<meta property="og:image:width" content="620"/>
	<meta property="og:image:height" content="541"/>
	<!-- Twitter Meta Tags -->
	<meta name="twitter:card" content="summary_large_image">
	<meta name="twitter:title" content="<?php echo $meta['meta_title']; ?>">
	<meta name="twitter:description" content="<?php echo $meta['meta_description']; ?>">
	<meta name="twitter:image" content="<?php echo isset($ogImage) ? $ogImage : base_url() . 'partage.jpg' ?>">
	<link rel="shortcut icon" href="<?php echo base_url(); ?>favicon.ico" type="image/x-icon">
	<link rel="icon" href="<?php echo base_url(); ?>favicon.ico" type="image/x-icon">
	<link rel="stylesheet" type="text/css" href="<?php css_file('bootstrap.css') ?>"/>
	<link rel="stylesheet" type="text/css" href="<?php css_file('animate.css') ?>"/>
	<link rel="stylesheet" type="text/css" href="<?php css_file('font-awesome.css') ?>"/>
	<link rel="stylesheet" type="text/css" href="<?php css_file('furniture-icons.css') ?>"/>
	<link rel="stylesheet" type="text/css" href="<?php css_file('linear-icons.css') ?>"/>
	<link rel="stylesheet" type="text/css" href="<?php css_file('magnific-popup.css') ?>"/>
	<link rel="stylesheet" type="text/css" href="<?php css_file('owl.carousel.css') ?>"/>
	<link rel="stylesheet" type="text/css" href="<?php css_file('ion-range-slider.css') ?>"/>
	<link rel="stylesheet" type="text/css" href="<?php css_file('theme.css') ?>"/>
	<!--Google fonts-->
	<link href="https://fonts.googleapis.com/css?family=Montserrat:400,500,600&amp;subset=latin-ext" rel="stylesheet">
	<link href="https://fonts.googleapis.com/css?family=Roboto:300,400,500" rel="stylesheet">
	<!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
	<!--[if lt IE 9]>
	<script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
	<script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
	<![endif]-->
</head>
<body>
<div class="page-loader"></div>
<div class="wrapper">
	<!-- ======================== Navigation ======================== -->
	<nav class="navbar-fixed">
		<div class="container">
			<div class="navigation navigation-top clearfix">
				<ul>
					<li><a target="_blank" href="https://www.facebook.com/tunisiameubleofficiel/"><i class="fa fa-facebook"></i></a></li>
					<li><a target="_blank" href="mailto:contact@tunisie-meuble.com"><i class="fa fa-envelope"></i></a></li>
					<li><a target="_blank" href="tel:(+216) 24 005 160"><i class="fa fa-phone"></i></a></li>
					<li><a href="<?php echo base_url() . 'login' ?>"><i class="icon icon-user"></i></a></li>
				</ul>
			</div> <!--/navigation-top-->
			<!-- ==========  Main navigation ========== -->
			<div class="navigation navigation-main">
				<!-- Setup your logo here-->
				<a href="<?php echo base_url(); ?>" class="logo"><img style="width: 141px;height: 43px;" src="<?php img_file('logo.png'); ?>" alt=""/></a>
				<!-- Mobile toggle menu -->
				<a href="#" class="open-menu"><i class="icon icon-menu"></i></a>
				<!-- Convertible menu (mobile/desktop)-->
				<div class="floating-menu">
					<!-- Mobile toggle menu trigger-->
					<div class="close-menu-wrapper">
						<span class="close-menu"><i class="icon icon-cross"></i></span>
					</div>
					<ul class="navbar-menu">
						<li class="<?php if($current_url === null){ echo "active"; } ?>"><a href="<?php echo base_url(); ?>">Accueil</a></li>
						<li class="<?php if($current_url === 'apropos'){ echo "active"; } ?>"><a href="<?php echo base_url() . 'apropos'; ?>">À propos</a></li>
						<li class="<?php if($current_url === 'produits'){ echo "active"; } ?>">
							<a href="<?php echo base_url() . 'produits'; ?>">Nos Produits <!--span class="open-dropdown"><i class="fa fa-angle-down"></i></span--></a>
							<!--div class="navbar-dropdown">
								<div class="navbar-box">
									<div class="box-2">
										<div class="box clearfix">
											<div class="row">
												<div class="col-md-4">
													<ul>
														<li class="label">Homepage</li>
													</ul>
												</div>
												<div class="col-md-4">
													<ul>
														<li><a href="">Blog article</a></li>
													</ul>
												</div>
												<div class="col-md-4">
													<ul>
														<li class="label">Pages</li>
													</ul>
													<ul>
														<li class="label">Extras</li>
													</ul>
												</div>
											</div>
										</div>
									</div>
								</div>
							</div-->
						</li>
						<li class="<?php if($current_url === 'commander'){ echo "active"; } ?>"><a href="<?php echo base_url() . 'commander'; ?>">Commande rapide</a></li>
						<li class="<?php if($current_url === 'blog'){ echo "active"; } ?>"><a href="<?php echo base_url() . 'blog'; ?>">Blog</a></li>
						<li class="<?php if($current_url === 'contact'){ echo "active"; } ?>"><a href="<?php echo base_url() . 'contact'; ?>">Contact</a></li>
					</ul>
				</div> <!--/floating-menu-->
			</div> <!--/navigation-main-->
		</div> <!--/container-->
	</nav>
	<!-- ========================  Icons slider ======================== -->
