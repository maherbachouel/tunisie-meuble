<?php $CI =& get_instance();
$CI->load->model('category_model', 'category');
$CI->load->model('blog_model', 'blog');
$category = $CI->category->read('*', array(), 5, 0);
$articles = $CI->blog->read('*', array("status" => 1), 2, 0);
?>
<footer>
	<div class="container">

		<!--footer showroom-->
		<div class="footer-showroom">
			<div class="row">
				<div class="col-sm-8">
					<h2>VISITEZ NOTRE SHOWROOM</h2>
					<p>Tunisie meuble, Avenue Cité Sportive, La Soukra</p>
					<p>Lun - Dim: 9:00 - 19:00</p>
				</div>
				<div class="col-sm-4 text-center">
					<a href="https://goo.gl/maps/VmbkBexTDzWb3Cy98" target="_blank" class="btn btn-clean"><span class="icon icon-map-marker"></span> Obtenir des directions</a>
					<div class="call-us h4"><span class="icon icon-phone-handset"></span>(+216) 24 005 160</div>
					<div class="call-us h4"><span class="icon icon-phone-handset"></span>(+216) 25 602 380</div>
				</div>
			</div>
		</div>

		<!--footer links-->
		<div class="footer-links">
			<div class="row">
				<div class="col-sm-4 col-md-2">
					<h5>Lien rapide</h5>
					<ul>
						<li><a href="<?php echo base_url() . 'contact'; ?>">Contact</a></li>
						<li><a href="<?php echo base_url() . 'blog'; ?>">Blog</a></li>
						<li><a target="_blank" href="https://goo.gl/maps/VmbkBexTDzWb3Cy98">Map</a></li>
						<li><a target="_blank" href="https://www.facebook.com/tunisiameubleofficiel/">Facebook</a></li>
					</ul>
				</div>
				<div class="col-sm-4 col-md-2">
					<h5>Catégories</h5>
					<ul>
						<?php if (!empty($category)) {
							foreach ($category as $cat) { ?>
								<li>
									<a href="<?php echo base_url() . 'produits/' . $cat->permalink; ?>">
										<?php echo $cat->name; ?>
									</a>
								</li>
							<?php }
						} ?>

					</ul>
				</div>
				<div class="col-sm-4 col-md-2">
					<h5>Article</h5>
					<ul>
						<?php if (!empty($articles)) {
							foreach ($articles as $article) { ?>
								<li>
									<a href="<?php echo base_url() . 'blog/' . $article->permalink; ?>">
										<?php echo $article->title; ?>
									</a>
								</li>
							<?php }
						} ?>
					</ul>
				</div>
				<div class="col-sm-12 col-md-6">
					<h5>INSCRIVEZ-VOUS À NOTRE NEWSLETTER</h5>
					<p><i>Ajoutez votre adresse e-mail pour vous inscrire à nos e-mails mensuels et recevoir des offres promotionnelles.</i></p>
					<div class="form-group form-newsletter">
						<form id="subscribe-newsletter-form" method="post" action="<?php echo base_url().'home/subscribe'?>">
							<input class="form-control" id="newsletter-input" type="email" required name="email" placeholder="Adresse email"/>
							<input type="submit" class="btn btn-clean btn-sm" value="Souscrire"/>
						</form>
						<div id="error-subscribe" class="text-danger m-t-15">
							<i aria-hidden="true" class="fa fa-arrow-right"></i> Vous êtes déjà abonné à la newsletter
						</div>
						<div id="success-subscribe" class="text-success m-t-15">
							<i aria-hidden="true" class="fa fa-arrow-right"></i> Merci de vous être abonné à notre newsletter
						</div>
					</div>
				</div>
			</div>
		</div>
		<!--footer social-->
		<div class="footer-social">
			<div class="row">
				<div class="col-sm-6">
					© 2020 Tunisie Meuble
				</div>
				<div class="col-sm-6 links">
					<ul>
						<li><a target="_blank" href="https://www.facebook.com/tunisiameubleofficiel/"><i class="fa fa-facebook"></i></a></li>
						<li><a target="_blank" href="mailto:contact@tunisie-meuble.com"><i class="fa fa-envelope"></i></a></li>
						<li><a target="_blank" href="tel:(+216) 24 005 160"><i class="fa fa-phone"></i></a></li>
					</ul>
				</div>
			</div>
		</div>
	</div>
</footer>
<script>
    window.base_url = "<?php echo base_url();?>";
</script>
<script type="text/javascript" src="<?php js_file('jquery.min.js') ?>"></script>
<script type="text/javascript" src="<?php js_file('jquery.bootstrap.js') ?>"></script>
<script type="text/javascript" src="<?php js_file('jquery.magnific-popup.js') ?>"></script>
<script type="text/javascript" src="<?php js_file('jquery.owl.carousel.js') ?>"></script>
<script type="text/javascript" src="<?php js_file('jquery.ion.rangeSlider.js') ?>"></script>
<script type="text/javascript" src="<?php js_file('jquery.isotope.pkgd.js') ?>"></script>
<script type="text/javascript" src="<?php js_file('main.js') ?>"></script>
<script type="text/javascript" src="<?php js_file('custom.js') ?>"></script>
</body>
</html>

