<!-- ========================  Main header ======================== -->

<section class="main-header" style="background-image:url(<?php img_file('gallery-2.jpg'); ?>)">
	<header>
		<div class="container text-center">
			<h1 class="h2 title">Blog</h1>
			<ol class="breadcrumb breadcrumb-inverted">
				<li><a href="<?php echo base_url(); ?>"><span class="icon icon-home"></span> Accueil</a></li>
				<li><a href="<?php echo base_url() . 'blog' ?>">Blog</a></li>
			</ol>
		</div>
	</header>
</section>

<!-- ========================  Blog ======================== -->
<section class="blog">
	<div class="container">
		<div class="pre-header">
			<div>
				<h2 class="h3 title">
					Nos articles
				</h2>
			</div>
			<div>
				<div class="sort-bar pull-right">
					<div class="sort-results">
						<!--Items counter-->
						<span>Tout afficher <strong>50</strong> de <strong>3,250</strong> articles</span>
						<!--Showing result per page-->
						<select>
							<option value="1">10</option>
							<option value="2">50</option>
							<option value="3">100</option>
							<option value="4">All</option>
						</select>
					</div>
				</div>
			</div>
		</div>
		<div class="row">
			<!-- === blog-item === -->

			<?php if (isset($articles)) {
				if (count($articles)) {
					foreach ($articles as $article) { ?>
						<div class="col-sm-4">
							<article>
								<a href="<?php echo base_url() . 'blog/' . $article->permalink; ?>">
									<div class="image" style="background-image:url(<?php echo base_url() . $article->banner; ?>)">
										<img src="<?php echo base_url() . $article->banner; ?>" alt="<?php echo $article->title; ?>"/>
									</div>
									<div class="entry entry-table">
										<div class="date-wrapper">
											<div class="date">
												<span><?php echo date("M", strtotime($article->created_at)); ?></span>
												<strong><?php echo date("d", strtotime($article->created_at)); ?></strong>
												<span><?php echo date("Y", strtotime($article->created_at)); ?></span>
											</div>
										</div>
										<div class="title">
											<h2 class="h5"><?php echo $article->title; ?></h2>
										</div>
									</div>
								</a>
							</article>
						</div>
					<?php }
				} else { ?>
					<div class="alert alert-warning text-center" role="alert">
						Aucun article trouvé
					</div>
				<?php } ?>
			<?php } ?>

		</div>
	</div>
</section>
