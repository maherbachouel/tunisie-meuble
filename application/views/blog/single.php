<!-- ========================  Main header ======================== -->

<section class="main-header main-header-blog" style="background-image:url(<?php img_file('gallery-1.jpg'); ?>)">
	<header>
		<div class="container text-center">
			<h2 class="h2 title">Article</h2>
			<ol class="breadcrumb breadcrumb-inverted">
				<li><a href="<?php echo base_url(); ?>"><span class="icon icon-home"></span> Accueil</a></li>
				<li><a class="active" href="#"><?php echo $article->title; ?></a></li>
			</ol>
		</div>
	</header>
</section>

<!-- ========================  Blog article ======================== -->
<section class="blog">
	<div class="container">

		<div class="row">

			<div class="col-sm-10 col-sm-offset-1 col-md-8 col-md-offset-2">

				<div class="blog-post">

					<!-- === blog main image & entry info === -->

					<div class="blog-image-main">
						<img src="<?php img_file('blog-1.jpg'); ?>" alt=""/>
					</div>

					<div class="blog-post-content">

						<!-- === blog post title === -->

						<div class="blog-post-title">
							<h1 class="blog-title">
								<?php echo $article->title; ?>
							</h1>
							<h2 class="blog-subtitle h5">
								You’ve finally reached this point in your life- you’ve bought your first home, moved into
								your very own apartment, moved out of the dorm room or you’re finally downsizing after
								all of your kids have left the nest.
							</h2>

							<div class="blog-info blog-info-top">
								<div class="entry">
									<i class="fa fa-user"></i>
									<span>Administrateur</span>
								</div>
								<div class="entry">
									<i class="fa fa-calendar"></i>
									<span><?php echo date("d-M-Y H:m:s", strtotime($article->created_at)); ?></span>
								</div>
							</div> <!--/blog-info-->
						</div>

						<!-- === blog post text === -->

						<div class="blog-post-text">
							<?php echo $article->description; ?>

						</div>

						<!-- === blog info === -->
					</div>
				</div><!--blog-post-->
			</div><!--col-sm-8-->
		</div> <!--/row-->
	</div><!--/container-->
</section>
