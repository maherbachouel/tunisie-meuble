<section class="not-found">
	<div class="container">
		<h1 class="title" data-title="Page not found!">404</h1>
		<div class="h4 subtitle">Sorry! Page not found.</div>
		<p>L'URL demandée est introuvable sur ce serveur. C'est tout ce que nous savons.</p>
		<p> <a href="<?php echo base_url(); ?>">Cliquez ici</a> pour accéder à la première page? </p>
	</div>
</section>
