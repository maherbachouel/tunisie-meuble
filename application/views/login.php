<!-- ========================  Main header ======================== -->
<section class="main-header" style="background-image:url(<?php img_file('gallery-2.jpg'); ?>)">
	<header>
		<div class="container text-center">
			<h2 class="h2 title">Connexion</h2>
			<ol class="breadcrumb breadcrumb-inverted">
				<li><a href="<?php echo base_url(); ?>"><span class="icon icon-home"></span> Accueil</a></li>
				<li><a class="active" href="<?php echo base_url() . 'login' ?>">Connexion</a></li>
			</ol>
		</div>
	</header>
</section>
<!-- ========================  Login & register ======================== -->
<section class="login-wrapper login-wrapper-page">
	<div class="container">
		<div class="row">

			<!-- === left content === -->

			<div class="col-md-6 col-md-offset-3">
				<!-- === login-wrapper === -->
				<div class="login-wrapper">
					<div class="white-block">
						<!--signin-->
						<div class="login-block">
							<div class="h4">Connexion</div>
							<hr/>
							<form action="<?php echo base_url() . 'login' ?>" method="post">
								<div class="row">
									<div class="col-md-12">
										<div class="form-group">
											<input required type="email" name="email" class="form-control" placeholder="Email">
										</div>
									</div>

									<div class="col-md-12">
										<div class="form-group">
											<input required type="password" name="password" class="form-control" placeholder="Mot de passe">
										</div>
									</div>
									<div class="text-center">
										<button type="submit" class="btn btn-main">Connexion</button>
									</div>
								</div>
							</form>
						</div> <!--/signin-->
						<!--signup-->
					</div>
				</div> <!--/login-wrapper-->
			</div> <!--/col-md-6-->
		</div>
	</div>
</section>
