<!-- ========================  Main header ======================== -->
<?php
$CI =& get_instance();
$CI->load->library('session');
?>
<section class="main-header" style="background-image:url(<?php img_file('gallery-2.jpg'); ?>)">
	<header>
		<div class="container text-center">
			<h2 class="h2 title">Contact</h2>
			<ol class="breadcrumb breadcrumb-inverted">
				<li><a href="<?php echo base_url(); ?>"><span class="icon icon-home"></span> Accueil</a></li>
				<li><a class="active" href="<?php echo base_url() . 'contact'; ?>">Contact</a></li>
			</ol>
		</div>
	</header>
</section>

<!-- ========================  Contact ======================== -->
<section class="contact">
	<!-- === Goolge map === -->
	<iframe src="https://www.google.com/maps/embed?pb=!1m14!1m8!1m3!1d3191.2716227897117!2d10.2051823!3d36.8838498!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x12e2cbe303f819bb%3A0x189c6c24d2957538!2sTunisie%20meuble!5e0!3m2!1sfr!2stn!4v1578649078647!5m2!1sfr!2stn" width="100%" height="450" frameborder="0" style="border:0;" allowfullscreen=""></iframe>
	<div class="container">
		<div class="row">

			<div class="col-sm-10 col-sm-offset-1 col-md-10 col-md-offset-1">

				<div class="contact-block">

					<div class="contact-info">
						<div class="row">
							<div class="col-sm-4">
								<figure class="text-center">
									<span class="icon icon-map-marker"></span>
									<figcaption>
										<strong>Adresse Showroom </strong>
										<span>Tunisie meuble, Avenue Cité Sportive, <br/>La Soukra</span><br/>
										<span><a href="mailto:contact@tunisie-meuble.com">contact@tunisie-meuble.com</a></span>
									</figcaption>
								</figure>
							</div>
							<div class="col-sm-4">
								<figure class="text-center">
									<span class="icon icon-phone"></span>
									<figcaption>
										<strong>Appelez-nous</strong>
										<span>
                                                     (+216) 24 005 160<br/>
                                                     (+216) 25 602 380
                                                </span>
									</figcaption>
								</figure>
							</div>
							<div class="col-sm-4">
								<figure class="text-center">
									<span class="icon icon-clock"></span>
									<figcaption>
										<strong>Horaires d’ouverture</strong>
										<span>
                                           Nous sommes ouverts du Lundi au Dimanche de 9h00 à 19h00 <br/>
                                         </span>
									</figcaption>
								</figure>
							</div>
						</div>
					</div>
					<div class="banner">
						<div class="row">
							<div class="col-md-offset-1 col-md-10 text-center">
								<h2 class="title">Besoin de conseils ou d’informations sur nos produits ou services ? </h2>
								<p>
									Contactez-nous par téléphone. Nous vous accueillons  tous les jours du lundi au dimanche dans votre magasin de meubles à la Soukra.
								</p>
								<?php
								if (!is_null($CI->session->success)) {
									if ($CI->session->success) { ?>
										<div class="alert alert-success" role="alert">
											Votre message a été envoyé avec succès
										</div>
									<?php } else { ?>
										<div class="alert alert-warning" role="alert">
											Votre message n'a pas pu été envoyé
										</div>
									<?php }
								} ?>
								<div class="contact-form-wrapper">

									<a class="btn btn-clean open-form" data-text-open="Contact us via form" data-text-close="Fermer le formulaire">Contactez-nous via le formulaire</a>

									<div class="contact-form clearfix">
										<form id="sendmail" name="sendmail" action="<?php echo base_url() . 'contact' ?>" method="post">
											<div class="row">
												<div class="col-md-6">
													<div class="form-group">
														<input name="name" required type="text" class="form-control" placeholder="Nom">
													</div>
												</div>

												<div class="col-md-6">
													<div class="form-group">
														<input name="email" required type="email" class="form-control" placeholder="Email">
													</div>
												</div>

												<div class="col-md-6">
													<div class="form-group">
														<input name="phone" required type="number" class="form-control" placeholder="Téléphone">
													</div>
												</div>
												<div class="col-md-6">
													<div class="form-group">
														<input name="subject" required type="text" class="form-control" placeholder="Sujet">
													</div>
												</div>

												<div class="col-md-12">
													<div class="form-group">
														<textarea name="message" required class="form-control" placeholder="Message" rows="10"></textarea>
													</div>
												</div>

												<div class="col-md-12 text-center">
													<input type="submit" class="btn btn-clean" value="Envoyer"/>
												</div>
											</div>
										</form>
									</div>
								</div>

							</div>
						</div>
					</div>
				</div>
			</div><!--col-sm-8-->
		</div> <!--/row-->
	</div><!--/container-->
</section>
