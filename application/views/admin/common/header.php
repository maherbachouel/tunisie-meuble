<!DOCTYPE html>
<html lang="fr">
<head>
	<meta charset="utf-8">
	<title>Admin - Tunisie Meuble</title>
	<meta name="description" content="Tunisie Meuble">
	<meta name="robots" content="noindex, nofollow">
	<meta name="theme-color" content="#bd0926" style="color: #bd0926;">
	<meta name="author" content="Maher Bachouel (maherbachouel@gmail.com)">
	<meta http-equiv="content-type" content="text/html;charset=UTF-8"/>
	<meta http-equiv="X-UA-Compatible" content="IE=11"/>
	<meta http-equiv="x-ua-compatible" content="ie=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
	<!-- Css files -->
	<link rel="shortcut icon" href="<?php echo base_url(); ?>favicon.ico" type="image/x-icon">
	<link rel="icon" href="<?php echo base_url(); ?>favicon.ico" type="image/x-icon">
	<link rel="stylesheet" type="text/css" href="<?php css_file('furniture-icons.css') ?>"/>
	<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.8.1/css/all.css" crossorigin="anonymous">
	<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
	<link href="<?php asset('admin/js/vendors/datatables/dataTables.bootstrap.min.css') ?>" rel="stylesheet">
	<link href="<?php asset('admin/js/vendors/bootstrap-datepicker/bootstrap-datepicker.min.css') ?>" rel="stylesheet">
	<link href="<?php asset('admin/js/vendors/select2/select2.css') ?>" rel="stylesheet">
	<link rel="stylesheet" type="text/css" href="<?php asset('admin/css/app.min.css') ?>"/>
	<link rel="stylesheet" type="text/css" href="<?php asset('admin/css/style.css') ?>"/>
	<link rel="stylesheet" type="text/css" href="<?php asset('admin/css/sweetalert2.min.css') ?>"/>
</head>
<body>
<!-- Header START -->
<div class="app">
	<div class="layout">
		<div class="header">
			<div class="logo logo-dark">
				<a href="<?php echo base_url() . 'admin/dashboard'; ?>">
					<img class="logo-header" src="<?php asset('admin/images/logo/logo2.png'); ?>" alt="Logo">
					<img class="logo-fold" src="<?php asset('admin/images/logo/favicon.ico'); ?>" alt="Logo">
				</a>
			</div>
			<div class="nav-wrap">
				<ul class="nav-left">
					<li class="desktop-toggle ">
						<a href="javascript:void(0);">
							<i class="anticon"></i>
						</a>
					</li>
					<li class="mobile-toggle">
						<a href="javascript:void(0);">
							<i class="anticon"></i>
						</a>
					</li>
				</ul>
				<ul class="nav-right">
					<li class="dropdown dropdown-animated scale-left">
						<div class="pointer" data-toggle="dropdown">
							<div class="avatar avatar-image  m-h-10 m-r-15">
								<img src="<?php asset('admin/images/avatars/default.jpg'); ?>" alt="">
							</div>
						</div>
						<div class="p-b-15 p-t-20 dropdown-menu pop-profile">
							<div class="p-h-20 p-b-15 m-b-10 border-bottom">
								<div class="d-flex m-r-50">
									<div class="avatar avatar-lg avatar-image">
										<img src="<?php asset('admin/images/avatars/default.jpg'); ?>" alt="">
									</div>
									<div class="m-l-10">
										<p class="m-b-0 text-dark font-weight-semibold user-name">Admin</p>
										<p class="m-b-0 opacity-07">Administrateur</p>
									</div>
								</div>
							</div>
							<a href="<?php echo base_url() . 'admin/profile' ?>" class="dropdown-item d-block p-h-15 p-v-10">
								<div class="d-flex align-items-center justify-content-between">
									<div>
										<i class="anticon opacity-04 font-size-16 anticon-user"></i>
										<span class="m-l-10">Profil</span>
									</div>
									<i class="anticon font-size-10 anticon-right"></i>
								</div>
							</a>
							<a href="<?php echo base_url() . 'login/logout' ?>" class="dropdown-item d-block p-h-15 p-v-10">
								<div class="d-flex align-items-center justify-content-between">
									<div>
										<i class="anticon opacity-04 font-size-16 anticon-logout"></i>
										<span class="m-l-10">Se déconnecter</span>
									</div>
									<i class="anticon font-size-10 anticon-right"></i>
								</div>
							</a>
						</div>
					</li>
				</ul>
			</div>

		</div>
		<!-- Header END -->
		<!-- Side Nav START -->
		<div class="side-nav">
			<div class="side-nav-inner">
				<ul class="side-nav-menu scrollable">
					<li class="nav-item dropdown dashboard <?php if ($activepage === "dashboard") {
						echo " active open";
					} ?>">
						<a href="<?php echo base_url() . 'admin/dashboard'; ?>">
                                <span class="icon-holder">
                                    <i class="anticon anticon-dashboard"></i>
                                </span>
							<span class="title">Tableau de bord</span>
						</a>
					</li>
					<li class="nav-item dropdown <?php if ($activepage === "category") {
						echo " active open";
					} ?>">
						<a class="dropdown-toggle" href="javascript:void(0);">
                                <span class="icon-holder">
                                  <i class="anticon anticon-tags"></i>
                                </span>
							<span class="title">Gestion des catégorie</span>
							<span class="arrow">
                                    <i class="arrow-icon"></i>
                                </span>
						</a>
						<ul class="dropdown-menu">
							<li class="">
								<a href="<?php echo base_url() . 'admin/category' ?>">Liste des catégorie</a>
							</li>
							<li class="">
								<a href="<?php echo base_url() . 'admin/category/add' ?>">Ajouter une catégorie</a>
							</li>
						</ul>
					</li>
					<li class="nav-item dropdown <?php if ($activepage === "product") {
						echo " active open";
					} ?>">
						<a class="dropdown-toggle" href="javascript:void(0);">
                                <span class="icon-holder">
                                 <i class="anticon anticon-shopping-cart"></i>
                                </span>
							<span class="title">Gestion des produits</span>
							<span class="arrow">
                                    <i class="arrow-icon"></i>
                                </span>
						</a>
						<ul class="dropdown-menu">
							<li class="">
								<a href="<?php echo base_url() . 'admin/product' ?>">Liste des produits</a>
							</li>
							<li class="">
								<a href="<?php echo base_url() . 'admin/product/add' ?>">Ajouter un produit</a>
							</li>
						</ul>
					</li>

					<li class="nav-item dropdown <?php if ($activepage === "order") {
						echo " active open";
					} ?>">
						<a class="dropdown-toggle" href="javascript:void(0);">
                                <span class="icon-holder">
									<i class="anticon anticon-credit-card"></i>
                                </span>
							<span class="title">Gestion des commandes</span>
							<span class="arrow">
                                    <i class="arrow-icon"></i>
                                </span>
						</a>
						<ul class="dropdown-menu">
							<li class="">
								<a href="<?php echo base_url() . 'admin/order' ?>">Liste des commandes</a>
							</li>
							<li class="">
								<a href="<?php echo base_url() . 'admin/order/add' ?>">Ajouter une commande</a>
							</li>
						</ul>
					</li>
					<li class="nav-item dropdown <?php if ($activepage === "blog") {
						echo " active open";
					} ?>">
						<a class="dropdown-toggle" href="javascript:void(0);">
                                <span class="icon-holder">
                                    <i class="anticon anticon-book"></i>
                                </span>
							<span class="title">Gestion des articles</span>
							<span class="arrow">
                                    <i class="arrow-icon"></i>
                                </span>
						</a>
						<ul class="dropdown-menu">
							<li class="">
								<a href="<?php echo base_url() . 'admin/blog' ?>">Liste des articles</a>
							</li>
							<li class="">
								<a href="<?php echo base_url() . 'admin/blog/add' ?>">Ajouter un article</a>
							</li>
						</ul>
					</li>
					<li class="nav-item dropdown <?php if ($activepage === "page") {
						echo " active open";
					} ?>">
						<a class="dropdown-toggle" href="javascript:void(0);">
                                <span class="icon-holder">
                                   <i class="anticon anticon-file"></i>
                                </span>
							<span class="title">Gestion des pages</span>
							<span class="arrow">
                                    <i class="arrow-icon"></i>
                                </span>
						</a>
						<ul class="dropdown-menu">
							<li class="">
								<a href="<?php echo base_url() . 'admin/page' ?>">Liste des pages</a>
							</li>
							<li class="">
								<a href="<?php echo base_url() . 'admin/page/add' ?>">Ajouter une page</a>
							</li>
						</ul>
					</li>
				</ul>
			</div>
		</div>
		<div class="page-container">
			<!-- Content Wrapper START -->
			<div class="main-content">
				<div class="row">
