</div>
</div>
<div class="d-none d-md-flex  p-h-40 justify-content-between">
	<span class="">© 2020 Tunisie Meuble</span>
</div>
</div>
</div>
</div>
<script>
    window.base_url = "<?php echo base_url();?>";
</script>
<script type="text/javascript" src="<?php asset('admin/js/vendors.min.js') ?>"></script>
<script type="text/javascript" src="<?php asset('admin/js/vendors/chartjs/Chart.min.js') ?>"></script>
<script type="text/javascript" src="<?php asset('admin/js/vendors/datatables/jquery.dataTables.min.js') ?>"></script>
<script type="text/javascript" src="<?php asset('admin/js/vendors/datatables/dataTables.bootstrap.min.js') ?>"></script>
<script type="text/javascript" src="<?php asset('admin/js/vendors/bootstrap-datepicker/bootstrap-datepicker.min.js') ?>"></script>
<script type="text/javascript" src="<?php asset('admin/js/vendors/jquery-validation/jquery.validate.min.js') ?>"></script>
<script type="text/javascript" src="<?php asset('admin/js/vendors/select2/select2.min.js') ?>"></script>
<script type="text/javascript" src="<?php asset('admin/js/app.min.js') ?>"></script>
<script type="text/javascript" src="<?php asset('admin/js/main.js') ?>"></script>
<script type="text/javascript" src="<?php asset('admin/js/sweetalert2.min.js') ?>"></script>
<script src="<?php asset('admin/js/jquery.slugify.js'); ?>"></script>
<script src="<?php asset('admin/js/vendors/ckeditor/ckeditor.js'); ?>"></script>
<script src="<?php asset('admin/js/vendors/ckeditor/adapters/jquery.js'); ?>"></script>
</body>
</html>
