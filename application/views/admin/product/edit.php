<div class="card">
	<div class="card-body">
		<h4>Modifier un produit</h4>
		<div class="m-t-25">
			<div class="">
				<form method="post" enctype="multipart/form-data" action="<?php if (!empty($product)) {
					echo base_url() . 'admin/product/edit/' . $product->id;
				} ?>">
					<div class="form-group">
						<div class="col-md-12">
							<label>Nom</label>
							<input required type="text" name="name" value="<?php echo $product->name; ?>" class="form-control m-b-15" placeholder="Nom" data-slugify="#category-slug">
						</div>
						<div class="col-md-12">
							<label>Lien</label>
							<input required type="text" name="permalink" value="<?php echo $product->permalink; ?>" id="category-slug" class="form-control m-b-15" placeholder="Lien">
						</div>
						<div class="col-md-12">
							<label>Meta description</label>
							<input required type="text" name="meta_description" value="<?php echo $product->meta_description; ?>" class="form-control m-b-15" placeholder="Meta description">
						</div>
						<div class="col-md-12">
							<label>Prix</label>
							<input type="text" required name="price" value="<?php echo $product->price; ?>" class="form-control m-b-15" placeholder="Prix">
						</div>
						<div class="col-md-12 m-b-15">
							<label>Catégorie</label>
							<select class="select2" name="category_id" required>
								<?php if (!empty($category)) {
									foreach ($category as $cat) { ?>
										<option value="<?php echo $cat->id; ?>" <?php if ($cat->id === $product->category_id) {
											echo ' selected';
										} ?>><?php echo $cat->name; ?></option>
									<?php }
								} ?>
							</select>
						</div>
						<div class="col-md-12 m-b-15">
							<label>Status</label>
							<select class="select2" name="status" required>
								<option value="1" <?php if ($product->status === '1') {
									echo ' selected';
								} ?>>Activer
								</option>
								<option value="0" <?php if ($product->status === '0') {
									echo ' selected';
								} ?>>Désactiver
								</option>
							</select>
						</div>
						<div class="col-md-12">
							<div class="custom-file">
								<input type="file" class="custom-file-input" name="image" id="customFile">
								<label class="custom-file-label" for="customFile">Image produit</label>
							</div>
							<img src="<?php echo base_url() . $product->banner; ?>" width="500" class="img-responsive" title="<?php echo $product->name; ?>">
						</div>
					</div>
					<div class="form-group text-right">
						<button class="btn btn-primary">Modifier</button>
					</div>
				</form>
			</div>
		</div>
	</div>
</div>
