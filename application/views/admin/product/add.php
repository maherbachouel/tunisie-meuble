<div class="card">
	<div class="card-body">
		<h4>Ajouter un produit</h4>
		<div class="m-t-25">
			<div class="">
				<form method="post" enctype="multipart/form-data" action="<?php echo base_url() . 'admin/product/add'; ?>">
					<div class="form-group">
						<div class="col-md-12">
							<label>Nom</label>
							<input required type="text" name="name" class="form-control m-b-15" placeholder="Nom" data-slugify="#category-slug">
						</div>
						<div class="col-md-12">
							<label>Lien</label>
							<input required type="text" name="permalink" id="category-slug" class="form-control m-b-15" placeholder="Lien">
						</div>
						<div class="col-md-12">
							<label>Meta description</label>
							<input required type="text" name="meta_description" class="form-control m-b-15" placeholder="Meta description">
						</div>
						<div class="col-md-12">
							<label>Prix</label>
							<input type="text" required name="price" class="form-control m-b-15" placeholder="Prix">
						</div>
						<div class="col-md-12 m-b-15">
							<label>Catégorie</label>
							<select class="select2" name="category_id" required>
								<?php if (!empty($category)) {
									foreach ($category as $cat) { ?>
										<option value="<?php echo $cat->id; ?>"><?php echo $cat->name; ?></option>
									<?php }
								} ?>
							</select>
						</div>
						<div class="col-md-12 m-b-15">
							<label>Status</label>
							<select class="select2" name="status" required>
								<option value="1">Activer</option>
								<option value="0">Désactiver</option>
							</select>
						</div>
						<div class="col-md-12">
							<div class="custom-file">
								<input required type="file" class="custom-file-input" name="image" id="customFile">
								<label class="custom-file-label" for="customFile">Image produit</label>
							</div>
						</div>
					</div>
					<div class="form-group text-right">
						<button class="btn btn-primary">Ajouter</button>
					</div>
				</form>
			</div>
		</div>
	</div>
</div>
