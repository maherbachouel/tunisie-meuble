<?php
$ci =& get_instance();
?>
<div class="card">
	<div class="card-body">
		<div class="d-flex justify-content-between align-items-center">
			<h4>Liste des produits</h4>
		</div>
		<div class="m-t-25">
			<div class="table-responsive">
				<table class="table data-table">
					<thead>
					<tr>
						<th>Image</th>
						<th>Référence</th>
						<th>Nom</th>
						<th>Prix</th>
						<th>Action</th>
					</tr>
					</thead>
					<tbody>
					<?php if (!empty($products)) {
						foreach ($products as $product) { ?>
							<tr>
								<td><img src="<?php echo base_url() . $product->banner; ?>" width="80" class="img-responsive" title="<?php echo $product->name; ?>"></td>
								<td><?php echo 'RTM'.$product->id; ?></td>
								<td><?php echo $product->name; ?></td>
								<td><?php echo $product->price.' TND'; ?></td>
								<td>
									<a href="<?php echo base_url() . 'admin/product/edit/' . $product->id; ?>"><i class="anticon anticon-edit"></i></a>
									<a href="javascript:void(0)" data-href="<?php echo base_url() . 'admin/product/delete' ?>" class="delete-element" data-id="<?php echo $product->id; ?>"><i class="anticon anticon-delete"></i></a>
								</td>
							</tr>
						<?php }
					} ?>
					</tbody>
				</table>
			</div>
		</div>
	</div>
</div>
