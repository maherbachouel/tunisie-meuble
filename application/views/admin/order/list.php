<?php
$ci =& get_instance();
?>
<div class="card">
	<div class="card-body">
		<div class="d-flex justify-content-between align-items-center">
			<h4>Liste des commandes</h4>
		</div>
		<div class="m-t-25">
			<div class="table-responsive">
				<table class="table data-table">
					<thead>
					<tr>
						<th>Nom & prénom</th>
						<th>Email</th>
						<th>Téléphone</th>
						<th>Produit</th>
						<th>Prix</th>
						<th>Methode de paiement</th>
						<th>Statut</th>
						<th>Date de création</th>
						<th>Action</th>
					</tr>
					</thead>
					<tbody>
					<?php if (!empty($orders)) {
						foreach ($orders as $order) { ?>
							<tr>
								<td><?php echo $order->firstname . ' ' . $order->lastname; ?></td>
								<td><?php echo $order->email; ?></td>
								<td><?php echo $order->phone; ?></td>
								<td><?php echo $order->product_id ? $order->product_name : '-'; ?></td>
								<td><?php echo $order->order_price ? $order->order_price : '-'; ?></td>
								<td><?php echo $order->payment_method; ?></td>
								<td>
									<?php if ($order->status === '0') { ?>
										<span class="font-size-12 badge badge-warning">En attente</span>
									<?php } ?>
									<?php if ($order->status === '1') { ?>
										<span class="font-size-12 badge badge-success">Valider</span>
									<?php } ?>
									<?php if ($order->status === '2') { ?>
										<span class="font-size-12 badge badge-blue">Livrer</span>
									<?php } ?>
								</td>
								<td><?php echo date("d-M-Y H:m:s", strtotime($order->created_at)); ?></td>
								<td>
									<a href="<?php echo base_url() . 'admin/order/edit/' . $order->id; ?>"><i class="anticon anticon-edit"></i></a>
									<?php if ($order->status !== '2') { ?>
										<a href="javascript:void(0)" data-href="<?php echo base_url() . 'admin/order/delete' ?>" class="delete-element" data-id="<?php echo $order->id; ?>"><i class="anticon anticon-delete"></i></a>
									<?php } ?>
								</td>
							</tr>
						<?php }
					} ?>
					</tbody>
				</table>
			</div>
		</div>
	</div>
</div>
