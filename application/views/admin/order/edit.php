<div class="card">
	<div class="card-body">
		<h4>Modifier une commande</h4>
		<div class="m-t-25">
			<div class="">
				<form method="post" enctype="multipart/form-data" action="<?php if (!empty($order)) {
					echo base_url() . 'admin/order/edit/' . $order->id;
				} ?>">
					<div class="form-group">
						<div class="row">
							<div class="col-md-6">
								<label>Nom de la commande</label>
								<input type="text" name="name" value="<?php echo $order->name; ?>" class="form-control m-b-15" placeholder="Nom">
							</div>
							<div class="col-md-6">
								<label>Prénom</label>
								<input required type="text" name="firstname" value="<?php echo $order->firstname; ?>" class="form-control m-b-15" placeholder="Prénom">
							</div>
						</div>
						<div class="row">
							<div class="col-md-6">
								<label>Nom</label>
								<input required type="text" name="lastname" value="<?php echo $order->lastname; ?>" class="form-control m-b-15" placeholder="Nom">
							</div>
							<div class="col-md-6">
								<label>Email</label>
								<input required type="email" name="email" value="<?php echo $order->email; ?>" class="form-control m-b-15" placeholder="Email">
							</div>
						</div>
						<div class="row">
							<div class="col-md-6">
								<label>Address</label>
								<input required type="text" name="address" value="<?php echo $order->address; ?>" class="form-control m-b-15" placeholder="Address">
							</div>
							<div class="col-md-6">
								<label>Téléphone</label>
								<input required type="number" name="phone" value="<?php echo $order->phone; ?>" class="form-control m-b-15" placeholder="Téléphone">
							</div>
						</div>
						<div class="row">
							<div class="col-md-6">
								<label>Prix</label>
								<input type="text" required name="order_price" value="<?php echo $order->order_price; ?>" class="form-control m-b-15" placeholder="Prix">
							</div>

							<div class="col-md-6">
								<label>Avance</label>
								<input type="text" name="advance" value="<?php echo $order->advance; ?>" class="form-control m-b-15" placeholder="Avance">
							</div>
						</div>
						<div class="row">
							<div class="col-md-6">
								<label>Commentaire</label>
								<textarea type="text" name="comment" class="form-control m-b-15" placeholder="Commentaire"><?php echo $order->comment; ?>
								</textarea>
							</div>

							<div class="col-md-6">
								<label>Methode de paiement</label>
								<select class="select2" name="payment_method" required>
									<option value="CASH" <?php if ($order->status === 'CASH') {
										echo ' selected';
									} ?>>Paiement en espèces
									</option>
									<option value="CHEQUE" <?php if ($order->status === 'CHEQUE') {
										echo ' selected';
									} ?>>Paiement par chèque
									</option>
								</select>
							</div>
						</div>
						<div class="row">
							<div class="col-md-6 m-b-15">
								<label>Statut</label>
								<select class="select2" name="status" required>
									<option value="0" <?php if ($order->status === '0') {
										echo ' selected';
									} ?>>En attente
									</option>
									<option value="1" <?php if ($order->status === '1') {
										echo ' selected';
									} ?>>Valider
									</option>
									<option value="2" <?php if ($order->status === '2') {
										echo ' selected';
									} ?>>Livrer
									</option>
								</select>
							</div>
							<div class="col-md-6 m-b-15">
								<label>Couleur</label>
								<select class="select2" name="color">
									<option value="Gris">Gris</option>
								</select>
							</div>
						</div>
						<div class="col-md-12">
							<?php if ($order->file != null) { ?>
								<img src="<?php echo base_url() . $order->file; ?>" width="800" class="img-responsive" title="<?php echo $order->name; ?>">
							<?php } else { ?>
								<img src="<?php echo base_url() . $order->product_file; ?>" width="800" class="img-responsive" title="<?php echo $order->name; ?>">
							<?php } ?>
						</div>
						<div class="col-md-12">
							<div class="custom-file">
								<input type="file" class="custom-file-input" name="image" id="customFile">
								<label class="custom-file-label" for="customFile">Image de nouveau produit</label>
							</div>
						</div>
					</div>
					<div class="form-group text-right">
						<button class="btn btn-primary">Modifier</button>
					</div>
				</form>
			</div>
		</div>
	</div>
</div>
