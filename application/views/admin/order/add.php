<div class="card">
	<div class="card-body">
		<h4>Ajouter une commande</h4>
		<div class="m-t-25">
			<div class="">
				<form method="post" enctype="multipart/form-data" action="<?php echo base_url() . 'admin/order/add/'; ?>">
					<div class="form-group">
						<div class="row">
							<div class="col-md-6">
								<label>Nom de la commande</label>
								<input type="text" name="name" class="form-control m-b-15" placeholder="Nom">
							</div>
							<div class="col-md-6">
								<label>Prénom</label>
								<input required type="text" name="firstname" class="form-control m-b-15" placeholder="Prénom">
							</div>
						</div>
						<div class="row">
							<div class="col-md-6">
								<label>Nom</label>
								<input required type="text" name="lastname" class="form-control m-b-15" placeholder="Nom">
							</div>
							<div class="col-md-6">
								<label>Email</label>
								<input required type="email" name="email" class="form-control m-b-15" placeholder="Email">
							</div>
						</div>
						<div class="row">
							<div class="col-md-6">
								<label>Address</label>
								<input required type="text" name="address" class="form-control m-b-15" placeholder="Address">
							</div>
							<div class="col-md-6">
								<label>Téléphone</label>
								<input required type="number" name="phone" class="form-control m-b-15" placeholder="Téléphone">
							</div>
						</div>
						<div class="row">
							<div class="col-md-6">
								<label>Prix</label>
								<input type="text" required name="order_price" class="form-control m-b-15" placeholder="Prix">
							</div>
							<div class="col-md-6">
								<label>Avance</label>
								<input type="text" name="advance"  class="form-control m-b-15" placeholder="Avance">
							</div>
						</div>
						<div class="row">
							<div class="col-md-6">
								<label>Commentaire</label>
								<textarea type="text" name="comment" class="form-control m-b-15" placeholder="Commentaire">
								</textarea>
							</div>
							<div class="col-md-6">
								<label>Methode de paiement</label>
								<select class="select2" name="payment_method" required>
									<option value="CASH">Paiement en espèces
									</option>
									<option value="CHEQUE">Paiement par chèque
									</option>
								</select>
							</div>
						</div>
						<div class="row">
							<div class="col-md-6 m-b-15">
								<label>Statut</label>
								<select class="select2" name="status" required>
									<option value="0" >En attente
									</option>
									<option value="1" >Valider
									</option>
									<option value="2" >Livrer
									</option>
								</select>
							</div>
							<div class="col-md-6 m-b-15">
								<label>Couleur</label>
								<select class="select2" name="color">
									<option value="Gris">Gris</option>
								</select>
							</div>
						</div>
						<div class="col-md-12">
							<div class="custom-file">
								<input type="file" class="custom-file-input" name="image" id="customFile">
								<label class="custom-file-label" for="customFile">Image de produit</label>
							</div>
						</div>
					</div>
					<div class="form-group text-right">
						<button class="btn btn-primary">Ajouter</button>
					</div>
				</form>
			</div>
		</div>
	</div>
</div>
