<?php
$ci =& get_instance();
?>
<div class="card">
	<div class="card-body">
		<div class="d-flex justify-content-between align-items-center">
			<h4>Liste des categories</h4>
		</div>
		<div class="m-t-25">
			<div class="table-responsive">
				<table class="table data-table">
					<thead>
					<tr>
						<th>Icon</th>
						<th>Nom</th>
						<th>Lien</th>
						<th>Action</th>
					</tr>
					</thead>
					<tbody>
					<?php if (!empty($category)) {
						foreach ($category as $cat) { ?>
							<tr>
								<td><i class="font-size-50 <?php echo $cat->icon; ?>"></i></td>
								<td><?php echo $cat->name; ?></td>
								<td><?php echo $cat->permalink; ?></td>
								<td>
									<a href="<?php echo base_url() . 'admin/category/edit/' . $cat->id; ?>"><i class="anticon anticon-edit"></i></a>
									<a href="javascript:void(0)" data-href="<?php echo base_url() . 'admin/category/delete' ?>" class="delete-element" data-id="<?php echo $cat->id; ?>"><i class="anticon anticon-delete"></i></a>
								</td>
							</tr>
						<?php }
					} ?>
					</tbody>
				</table>
			</div>
		</div>
	</div>
</div>
