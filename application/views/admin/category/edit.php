<div class="card">
	<div class="card-body">
		<h4>Modifier une catégorie</h4>
		<div class="m-t-25">
			<div class="">
				<form method="post" action="<?php if (isset($category)) {
					echo base_url() . 'admin/category/edit/' . $category->id;
				} ?>">
					<div class="form-group">
						<div class="col-md-12">
							<label>Nom</label>
							<input required type="text" name="name" value="<?php echo $category->name; ?>" class="form-control m-b-15" placeholder="Nom" data-slugify="#category-slug">
						</div>
						<div class="col-md-12">
							<label>Lien</label>
							<input required type="text" name="permalink" value="<?php echo $category->permalink; ?>" id="category-slug" class="form-control m-b-15" placeholder="Lien">
						</div>
						<div class="col-md-12">
							<label>Meta description</label>
							<input required type="text" name="meta_description" value="<?php echo $category->meta_description; ?>" class="form-control m-b-15" placeholder="Meta description">
						</div>
						<div class="col-md-12">
							<label>Image</label>
							<select class="select2" name="icon" placeholder="Image" required>
								<option value="f-icon f-icon-sofa" <?php if ($category->icon === 'f-icon f-icon-sofa') {
									echo ' selected';
								} ?>><span>Canapé</option>
								<option value="f-icon f-icon-armchair" <?php if ($category->icon === 'f-icon f-icon-armchair') {
									echo ' selected';
								} ?>><span>Fauteuils</option>
								<option value="f-icon f-icon-chair" <?php if ($category->icon === 'f-icon f-icon-chair') {
									echo ' selected';
								} ?>><span>Chaises</option>
								<option value="f-icon f-icon-dining-table" <?php if ($category->icon === 'f-icon f-icon-dining-table') {
									echo ' selected';
								} ?>><span>Tables à manger</option>
								<option value="f-icon f-icon-media-cabinet" <?php if ($category->icon === 'f-icon f-icon-media-cabinet') {
									echo ' selected';
								} ?>><span>Stockage média</option>
								<option value="f-icon f-icon-table" <?php if ($category->icon === 'f-icon f-icon-table') {
									echo ' selected';
								} ?>><span>Tables</option>
								<option value="f-icon f-icon-bookcase" <?php if ($category->icon === 'f-icon f-icon-bookcase') {
									echo ' selected';
								} ?>><span>Bibliothèque</option>
								<option value="f-icon f-icon-bedroom" <?php if ($category->icon === 'f-icon f-icon-bedroom') {
									echo ' selected';
								} ?>><span>Chambre</option>
								<option value="f-icon f-icon-nightstand" <?php if ($category->icon === 'f-icon f-icon-nightstand') {
									echo ' selected';
								} ?>><span>Table de nuit</option>
								<option value="f-icon f-icon-children-room" <?php if ($category->icon === 'f-icon f-icon-children-room') {
									echo ' selected';
								} ?>><span>Chambre d'enfants</option>
								<option value="f-icon f-icon-kitchen" <?php if ($category->icon === 'f-icon f-icon-kitchen') {
									echo ' selected';
								} ?>><span>Cuisine</option>
								<option value="f-icon f-icon-bathroom" <?php if ($category->icon === 'f-icon f-icon-bathroom') {
									echo ' selected';
								} ?>><span>Salle de bains</option>
								<option value="f-icon f-icon-wardrobe" <?php if ($category->icon === 'f-icon f-icon-wardrobe') {
									echo ' selected';
								} ?>><span>Garde-robe</option>
								<option value="f-icon f-icon-shoe-cabinet" <?php if ($category->icon === 'f-icon f-icon-shoe-cabinet') {
									echo ' selected';
								} ?>><span>Placard à chaussures</option>
								<option value="f-icon f-icon-office" <?php if ($category->icon === 'f-icon f-icon-office') {
									echo ' selected';
								} ?>><span>Bureau</option>
								<option value="f-icon f-icon-bar-set" <?php if ($category->icon === 'f-icon f-icon-bar-set') {
									echo ' selected';
								} ?>><span>Ensembles de barres</option>
								<option value="f-icon f-icon-lightning" <?php if ($category->icon === 'f-icon f-icon-lightning') {
									echo ' selected';
								} ?>><span>Foudre</option>
								<option value="f-icon f-icon-carpet" <?php if ($category->icon === 'f-icon f-icon-carpet') {
									echo ' selected';
								} ?>><span>Varpet</option>
								<option value="f-icon f-icon-accessories" <?php if ($category->icon === 'f-icon f-icon-accessories') {
									echo ' selected';
								} ?>><span>Accessoires</option>
								<option value="f-icon  f-icon-pack" <?php if ($category->icon === 'f-icon f-icon-pack') {
									echo ' selected';
								} ?>><span>Pack</option>
							</select>
						</div>
					</div>
					<div class="form-group text-right">
						<button class="btn btn-primary">Modifier</button>
					</div>
				</form>
			</div>
		</div>
	</div>
</div>
