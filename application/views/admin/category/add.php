<div class="card">
	<div class="card-body">
		<h4>Ajouter une catégorie</h4>
		<div class="m-t-25">
			<div class="">
				<form method="post" action="<?php echo base_url() . 'admin/category/add'; ?>">
					<div class="form-group">
						<div class="col-md-12">
							<label>Nom</label>
							<input required type="text" name="name" class="form-control m-b-15" placeholder="Nom" data-slugify="#category-slug">
						</div>
						<div class="col-md-12">
							<label>Lien</label>
							<input required type="text" name="permalink" id="category-slug" class="form-control m-b-15" placeholder="Lien">
						</div>
						<div class="col-md-12">
							<label>Meta description</label>
							<input required type="text" name="meta_description" class="form-control m-b-15" placeholder="Meta description">
						</div>
						<div class="col-md-12">
							<label>Image</label>
							<select class="select2" name="icon" placeholder="Image" required>
								<option value="f-icon f-icon-sofa"><span>Canapé</option>
								<option value="f-icon f-icon-armchair"><span>Fauteuils</option>
								<option value="f-icon f-icon-chair"><span>Chaises</option>
								<option value="f-icon f-icon-dining-table"><span>Tables à manger</option>
								<option value="f-icon f-icon-media-cabinet"><span>Stockage média</option>
								<option value="f-icon f-icon-table"><span>Tables</option>
								<option value="f-icon f-icon-bookcase"><span>Bibliothèque</option>
								<option value="f-icon f-icon-bedroom"><span>Chambre</option>
								<option value="f-icon f-icon-nightstand"><span>Table de nuit</option>
								<option value="f-icon f-icon-children-room"><span>Chambre d'enfants</option>
								<option value="f-icon f-icon-kitchen"><span>Cuisine</option>
								<option value="f-icon f-icon-bathroom"><span>Salle de bains</option>
								<option value="f-icon f-icon-wardrobe"><span>Garde-robe</option>
								<option value="f-icon f-icon-shoe-cabinet"><span>Placard à chaussures</option>
								<option value="f-icon f-icon-office"><span>Bureau</option>
								<option value="f-icon f-icon-bar-set"><span>Ensembles de barres</option>
								<option value="f-icon f-icon-lightning"><span>Foudre</option>
								<option value="f-icon f-icon-carpet"><span>Varpet</option>
								<option value="f-icon f-icon-accessories"><span>Accessoires</option>
								<option value="f-icon  f-icon-pack"><span>Pack</option>
							</select>
						</div>
					</div>
					<div class="form-group text-right">
						<button class="btn btn-primary">Ajouter</button>
					</div>
				</form>
			</div>
		</div>
	</div>
</div>
