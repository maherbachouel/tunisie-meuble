<div class="card">
	<div class="card-body">
		<h4>Modifier un article</h4>
		<div class="m-t-25">
			<div class="">
				<div class="row">
					<div class="col-sm-12">
						<form role="form" enctype="multipart/form-data" method="post" class="form-horizontal" action="<?php echo base_url(); ?>admin/blog/edit/<?php echo $article->id ?>">
							<div class="panel panel-default">
								<div class="panel-body">
									<div class="form-group">
										<label class="col-sm-2 control-label">Titre</label>
										<div class="col-sm-10">
											<input type="text" class="form-control" name="title" value="<?php echo $article->title; ?>" required>
										</div>
									</div>
									<div class="form-group">
										<label class="col-sm-2 control-label">Description</label>
										<div class="col-sm-10">
											<textarea class="form-control ckeditor" rows="10" name="description" required><?php echo $article->description; ?> </textarea>
										</div>
									</div>

									<div class="form-group">
										<label class="col-sm-2 control-label">Status</label>
										<div class="col-sm-10">
											<select required class="form-control" name="status">
												<option value="1" <?php if ($article->status == 1) {
													echo " selected";
												} ?>>Publier
												</option>
												<option value="0" <?php if ($article->status == 0) {
													echo " selected";
												} ?>>Non Publier
												</option>
											</select>
										</div>
									</div>
									<div class="form-group">
										<label class="col-sm-2 control-label">Meta Description</label>
										<div class="col-sm-10">
											<input type="text" class="form-control" name="meta_description" value="<?php echo $article->meta_description; ?>" required>
										</div>
									</div>
									<div class="form-group">
										<label class="col-sm-2 control-label">Permalink</label>
										<div class="col-sm-10">
											<input type="text" class="form-control" name="permalink" value="<?php echo $article->permalink; ?>" required>
										</div>
									</div>
									<div class="form-group">
										<label class="col-sm-2 control-label">Image</label>
										<div class="col-sm-10">
											<input type="file" class="form-control" name="banner"
												   accept="image/png, image/jpeg, image/gif"/>
										</div>
									</div>
								</div>
							</div>
							<div class="form-group text-right">
								<button class="btn btn-primary">Modifier</button>
							</div>
						</form>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
