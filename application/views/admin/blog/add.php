<div class="card">
	<div class="card-body">
		<h4>Ajouter un article</h4>
		<div class="m-t-25">
			<div class="">
				<div class="row">
					<div class="col-sm-12">
						<form role="form" enctype="multipart/form-data" method="post" class="form-horizontal" action="<?php echo base_url(); ?>admin/blog/add">
							<div class="panel panel-default">
								<div class="panel-body">
									<div class="form-group">
										<label class="col-sm-2 control-label">Titre</label>
										<div class="col-sm-10">
											<input type="text" data-slugify="#blog-slug" class="form-control keyup-meta" name="title" placeholder="Titre" required>
										</div>
									</div>
									<div class="form-group">
										<label class="col-sm-2 control-label">Permalink</label>
										<div class="col-sm-10">
											<input type="text" id="blog-slug" class="form-control permalink-set" name="permalink" placeholder="Permalink" required>
										</div>
									</div>

									<div class="form-group">
										<label class="col-sm-2 control-label">Meta Description</label>
										<div class="col-sm-10">
											<input type="text" class="form-control" name="meta_description" placeholder="Meta Description" required>
										</div>
									</div>
									<div class="form-group">
										<label class="col-sm-2 control-label">Description</label>
										<div class="col-sm-10">
											<textarea class="form-control ckeditor" rows="10" name="description" required></textarea>
										</div>
									</div>

									<div class="form-group">
										<label class="col-sm-2 control-label">Status</label>
										<div class="col-sm-10">
											<select required class="form-control" name="status">
												<option value="1" selected>Publier</option>
												<option value="0">Non Publier</option>
											</select>
										</div>
									</div>
									<div class="form-group">
										<label class="col-sm-2 control-label">Image</label>
										<div class="col-sm-10">
											<input type="file" class="form-control" name="banner"
												   accept="image/png, image/jpeg, image/gif" required/>
										</div>
									</div>
								</div>
							</div>
							<div class="form-group text-right">
								<button class="btn btn-primary">Ajouter</button>
							</div>
						</form>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
