<?php
$ci =& get_instance();
?>
<div class="card">
	<div class="card-body">
		<div class="d-flex justify-content-between align-items-center">
			<h4>Liste des categories</h4>
		</div>
		<div class="m-t-25">
			<div class="table-responsive">
				<table class="table data-table">
					<thead>
					<tr>
						<th>Image</th>
						<th>Nom</th>
						<th>Lien</th>
						<th>Action</th>
					</tr>
					</thead>
					<tbody>
					<?php if (!empty($articles)) {
						foreach ($articles as $article) { ?>
							<tr>
								<td><img src="<?php echo base_url() . $article->banner; ?>" width="80" class="img-responsive" title="<?php echo $article->title; ?>"></td>
								<td><?php echo $article->title; ?></td>
								<td><?php echo $article->permalink; ?></td>
								<td>
									<a href="<?php echo base_url() . 'admin/blog/edit/' . $article->id; ?>"><i class="anticon anticon-edit"></i></a>
									<a href="javascript:void(0)" data-href="<?php echo base_url() . 'admin/blog/delete' ?>" class="delete-element" data-id="<?php echo $article->id; ?>"><i class="anticon anticon-delete"></i></a>
								</td>
							</tr>
						<?php }
					} ?>
					</tbody>
				</table>
			</div>
		</div>
	</div>
</div>
