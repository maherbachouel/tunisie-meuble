<?php
$ci =& get_instance();
?>
<div class="card">
	<div class="card-body">
		<div class="d-flex justify-content-between align-items-center">
			<h4>Liste des pages</h4>
		</div>
		<div class="m-t-25">
			<div class="table-responsive">
				<table class="table data-table">
					<thead>
					<tr>
						<th>Lien</th>
						<th>Meta title</th>
						<th>Meta description</th>
						<th>Action</th>
					</tr>
					</thead>
					<tbody>
					<?php if (!empty($pages)) {
						foreach ($pages as $page) { ?>
							<tr>
								<td><?php echo $page->slug; ?></td>
								<td><?php echo $page->meta_title; ?></td>
								<td><?php echo $page->meta_description; ?></td>
								<td>
									<a href="<?php echo base_url() . 'admin/page/edit/' . $page->id; ?>"><i class="anticon anticon-edit"></i></a>
									<a href="javascript:void(0)" data-href="<?php echo base_url() . 'admin/page/delete' ?>" class="delete-element" data-id="<?php echo $page->id; ?>"><i class="anticon anticon-delete"></i></a>
								</td>
							</tr>
						<?php }
					} ?>
					</tbody>
				</table>
			</div>
		</div>
	</div>
</div>
