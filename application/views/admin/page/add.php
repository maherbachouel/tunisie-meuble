<div class="card">
	<div class="card-body">
		<h4>Ajouter une page</h4>
		<div class="m-t-25">
			<div class="">
				<div class="row">
					<div class="col-sm-12">
						<form role="form" enctype="multipart/form-data" method="post" class="form-horizontal" action="<?php echo base_url(); ?>admin/page/add">
							<div class="panel panel-default">
								<div class="panel-body">
									<div class="form-group">
										<label class="col-sm-2 control-label">Titre</label>
										<div class="col-sm-10">
											<input type="text" class="form-control" name="title" placeholder="Titre" required>
										</div>
									</div>
									<div class="form-group">
										<label class="col-sm-2 control-label">Meta description</label>
										<div class="col-sm-10">
											<input type="text" class="form-control" name="meta_description" placeholder="Meta description" required>
										</div>
									</div>
									<div class="form-group">
										<label class="col-sm-2 control-label">Slug(name)</label>
										<div class="col-sm-10">
											<input type="text" class="form-control" name="slug" placeholder="Slug" required>
										</div>
									</div>
								</div>
							</div>
							<div class="form-group text-right">
								<button class="btn btn-primary">Ajouter</button>
							</div>
						</form>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
