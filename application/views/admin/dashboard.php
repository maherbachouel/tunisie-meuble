<?php
$CI =& get_instance();
?>
<div class="card">
	<div class="card-body">
		<div class="page-header no-gutters">
			<div class="d-md-flex align-items-md-center justify-content-between">
				<div class="media m-v-10 align-items-center">
					<div class="avatar avatar-image avatar-lg">
						<img src="<?php asset('admin/images/avatars/default.jpg'); ?>" alt="">
					</div>
					<div class="media-body m-l-15">
						<h4 class="m-b-0">Bienvenue, <?php echo $CI->current_user->firstname . ' ' . $CI->current_user->lastname; ?> </h4>
						<span class="text-gray">Administrateur</span>
					</div>
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-md-6 col-lg-6">
				<div class="card">
					<div class="card-body">
						<div class="d-flex justify-content-between align-items-center">
							<div>
								<p class="m-b-0">Commandes</p>
								<h2 class="m-b-0">
									<span><?php echo $orders; ?></span>
								</h2>
							</div>
							<div class="avatar avatar-icon avatar-lg avatar-blue">
								<i class="anticon anticon-dollar"></i>
							</div>
						</div>
					</div>
				</div>
			</div>
			<div class="col-md-6 col-lg-6">
				<div class="card">
					<div class="card-body">
						<div class="d-flex justify-content-between align-items-center">
							<div>
								<p class="m-b-0">Produits</p>
								<h2 class="m-b-0">
									<span><?php echo $products;?></span>
								</h2>
							</div>
							<div class="avatar avatar-icon avatar-lg avatar-cyan">
								<i class="anticon anticon-shopping-cart"></i>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
