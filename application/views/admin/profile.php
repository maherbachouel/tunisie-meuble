<?php
$CI =& get_instance();
?>
<div class="container">
	<div class="tab-content m-t-15">
		<div class="tab-pane fade show active" id="tab-account">
			<div class="card">
				<div class="card-header">
					<h4 class="card-title">Informations de base</h4>
				</div>
				<div class="card-body">
					<div class="media align-items-center">
						<div class="avatar avatar-image  m-h-10 m-r-15" style="height: 80px; width: 80px">
							<img src="<?php asset('admin/images/avatars/default.jpg'); ?>" alt="">
						</div>
						<div class="m-l-20 m-r-20">
							<h5 class="m-b-5 font-size-18"><?php echo $user->firstname . ' ' . $user->lastname; ?></h5>
							<p class="opacity-07 font-size-13 m-b-0">
								<?php echo $user->email; ?>
							</p>
						</div>
					</div>
					<hr class="m-v-25">
					<form method="post" action="<?php echo base_url() . 'admin/profile' ?>">
						<div class="form-row">
							<div class="form-group col-md-6">
								<label class="font-weight-semibold" for="userName">Nom</label>
								<input type="text" class="form-control" name="firstname" value="<?php echo $user->firstname ?>">
							</div>
							<div class="form-group col-md-6">
								<label class="font-weight-semibold" for="userName">Prénom</label>
								<input type="text" class="form-control" name="lastname" value="<?php echo $user->lastname; ?>">
							</div>

						</div>
						<div class="form-row">
							<div class="form-group col-md-6">
								<label class="font-weight-semibold" for="email">Email:</label>
								<input type="email" class="form-control" name="email" value="<?php echo $user->email; ?>">
							</div>
							<div class="form-group col-md-6">
								<label class="font-weight-semibold" for="dob">Mot de passe</label>
								<input type="text" class="form-control" name="password" value="<?php echo $CI->encryption->decrypt($user->password); ?>">
							</div>
						</div>
						<div class="form-row text-right">
							<div class="form-group col-md-12">
								<button class="btn btn-primary m-t-30">Modifier</button>
							</div>
						</div>
					</form>
				</div>
			</div>
		</div>
	</div>
