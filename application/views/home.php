<!-- ========================  Products widget ======================== -->

<!-- ========================  Header content ======================== -->
<section class="header-content">
	<div class="owl-slider">
		<!-- === slide item === -->
		<div class="item" style="background-image:url(<?php img_file('gallery-1.jpg'); ?>)">
			<div class="box">
				<div class="container">
					<h2 class="title animated h1" data-animation="fadeInDown">Tunisie Meuble, enfin des meubles qui vous ressemblent !</h2>
					<div class="animated" data-animation="fadeInUp">
						Chez Tunisie Meuble, <br>vous trouverez tout le mobilier pour votre salon, living ou séjour.
					</div>
					<div class="animated" data-animation="fadeInUp">
						<a href="<?php echo base_url() . 'produits' ?>" class="btn btn-clean">Tous les produits</a>
					</div>
				</div>
			</div>
		</div>
		<div class="item" style="background-image:url(<?php img_file('gallery-4.jpg'); ?>)">
			<div class="box">
				<div class="container">
					<h2 class="title animated h1" data-animation="fadeInDown">Un large choix de mobilier de cuisine et de salle à manger </h2>
					<div class="animated" data-animation="fadeInUp">
						Pour répondre aux goûts des plus petits !<br> Découvrez vite tous nos modèles de chambres enfants et bébé !
					</div>
					<div class="animated" data-animation="fadeInUp">
						<a href="<?php echo base_url() . 'produits' ?>" class="btn btn-clean">Tous les produits</a>
					</div>
				</div>
			</div>
		</div>

		<!-- === slide item === -->

		<div class="item" style="background-image:url(<?php img_file('gallery-2.jpg'); ?>)">
			<div class="box">
				<div class="container">
					<h2 class="title animated h1" data-animation="fadeInDown">Tunisie Meuble, un grand choix de salons, canapés et séjours</h2>
					<div class="animated" data-animation="fadeInUp">
						Des meubles de salons de haute qualité à des prix intéressants !
					</div>
					<div class="animated" data-animation="fadeInUp">
						<a href="<?php echo base_url() . 'produits' ?>" class="btn btn-clean">Tous les produits</a>
					</div>
				</div>
			</div>
		</div>

		<!-- === slide item === -->

		<div class="item" style="background-image:url(<?php img_file('gallery-3.jpg'); ?>)">
			<div class="box">
				<div class="container">
					<h2 class="title animated h1" data-animation="fadeInDown">
						Des meubles de salle à manger et cuisine pour tous les goûts !
					</h2>
					<div class="animated" data-animation="fadeInUp">
						Tunisie Meuble vous propose différents modèles de meubles pour salle à manger et cuisine pour créer des espaces
						<br>Agréables
						<br>Accueillants
						<br>Fonctionnels
					</div>
					<div class="animated" data-animation="fadeInUp">
						<a href="<?php echo base_url() . 'produits' ?>" class="btn btn-clean">Tous les produits</a>
					</div>
				</div>
			</div>
		</div>

	</div> <!--/owl-slider-->
</section>

<section class="owl-icons-wrapper owl-icons-frontpage">
	<!-- === header === -->

	<header class="hidden">
		<h2>Catégories de produits</h2>
	</header>
	<div class="container">
		<div class="owl-icons">
			<!-- === icon item === -->
			<?php if (!empty($category)) {
				foreach ($category as $cat) { ?>
					<a href="<?php echo base_url() . 'produits/' . $cat->permalink; ?>">
						<figure>
							<i class="<?php echo $cat->icon; ?>"></i>
							<figcaption><?php echo $cat->name; ?></figcaption>
						</figure>
					</a>
				<?php }
			} ?>
		</div> <!--/owl-icons-->
	</div> <!--/container-->
</section>
<section class="site-text text-center">
	<h1><strong>Tunisie Meuble,</strong> votre magasin de meubles en Tunisie</h1>
	<p><strong>Tunisie Meuble</strong> vous propose une boutique en ligne avec une grande sélection de meubles. En un clin d'œil, vous trouverez des articles et des <strong>meubles tendance</strong> pour votre intérieur. </p>
	<p>Nous proposons des<strong> meubles au design moderne</strong>, des prix imbattables, des matériaux de haute qualité et des nouveautés exclusives. </p>
	<p>Ceci et bien plus vous attend dans notre <strong>magasin de meubles en ligne</strong> et dans notre showroom situé à la Soukra.</p>
</section>
<section class="products">

	<div class="container">

		<!-- === header title === -->

		<header>
			<div class="row">
				<div class="col-md-offset-2 col-md-8 text-center">
					<h2 class="title">Popular products</h2>
					<div class="text">
						<p>Découvrez nos dernières collections</p>
					</div>
				</div>
			</div>
		</header>

		<div class="row">

			<!-- === product-item === -->
			<?php if (isset($products)) {
				if (count($products)) {
					foreach ($products as $product) { ?>
						<div class="col-md-4 col-sm-6 col-xs-12" style="margin-bottom: 15px">
							<article>
								<div class="info">
							<span>
							  <a href="<?php echo base_url() . 'produits/' . $product->category_permalink . '/' . $product->permalink; ?>" data-title="Aperçu rapide"><i class="icon icon-eye"></i></a>
							</span>
								</div>
								<div class="btn btn-add">
									<a href="<?php echo base_url() . 'commander/' . $product->id; ?>"><i class="icon icon-cart"></i></a>
								</div>
								<div class="figure-grid">
									<span class="label label-info"><?php echo 'RTM' . $product->id; ?></span>
									<div class="image">
										<a href="<?php echo base_url() . 'produits/' . $product->category_permalink . '/' . $product->permalink; ?>">
											<img src="<?php echo base_url() . $product->banner; ?>" alt="" width="360"/>
										</a>
									</div>
									<div class="text">
										<h2 class="title h4"><a href="<?php echo base_url() . 'produits/' . $product->category_permalink . '/' . $product->permalink; ?>"><?php echo $product->name; ?></a></h2>
										<sup><?php echo $product->price; ?> TND</sup>
									</div>
								</div>
							</article>
						</div>
					<?php }
				} else { ?>
					<div class="alert alert-warning text-center" role="alert">
						Aucune produit trouvé
					</div>
				<?php }
			} ?>
			<!-- === product-item === -->

		</div> <!--/row-->
		<!-- === button more === -->

		<div class="wrapper-more">
			<a href="<?php echo base_url() . 'produits'; ?>" class="btn btn-main">Voir magasin</a>
		</div>

		<!-- ========================  Product info popup - quick view ======================== -->

	</div> <!--/container-->
</section>
<!-- ========================  Stretcher widget ======================== -->
<section class="stretcher-wrapper">
	<!-- === stretcher header === -->
	<header class="hidden">
		<!--remove class 'hidden'' to show section header -->
		<div class="container">
			<div class="row">
				<div class="col-md-offset-2 col-md-8 text-center">
					<h1 class="h2 title">Catégories populaires</h1>
					<div class="text">
						<p>
							Que vous changiez de maison ou emménagiez dans une nouvelle, vous trouverez une vaste sélection de meubles de salon de qualité,
							meubles de chambre à coucher, meubles de salle à manger et la meilleure valeur à l'usine de meubles
						</p>
					</div>
				</div>
			</div>
		</div>
	</header>
	<!-- === stretcher === -->
	<ul class="stretcher">
		<!-- === stretcher item === -->
		<li class="stretcher-item" style="background-image:url(<?php img_file('gallery-1.jpg'); ?>);">
			<!--logo-item-->
			<div class="stretcher-logo">
				<div class="text">
					<span class="f-icon f-icon-bedroom"></span>
					<span class="text-intro">Chambre</span>
				</div>
			</div>
			<!--main text-->
			<figure>
				<h4>Projets d'ameublement moderne</h4>
				<figcaption>De nouvelles idées d'ameublement</figcaption>
			</figure>
			<!--anchor-->
		</li>

		<!-- === stretcher item === -->

		<li class="stretcher-item" style="background-image:url(<?php img_file('gallery-2.jpg'); ?>);">
			<!--logo-item-->
			<div class="stretcher-logo">
				<div class="text">
					<span class="f-icon f-icon-sofa"></span>
					<span class="text-intro">Salon</span>
				</div>
			</div>
			<!--main text-->
			<figure>
				<h4>Ameublement et compléments</h4>
				<figcaption>Découvrez la collection de tables design</figcaption>
			</figure>
			<!--anchor-->
		</li>
		<!-- === stretcher item === -->
		<li class="stretcher-item" style="background-image:url(<?php img_file('gallery-3.jpg'); ?>);">
			<!--logo-item-->
			<div class="stretcher-logo">
				<div class="text">
					<span class="f-icon f-icon-office"></span>
					<span class="text-intro">Bureau</span>
				</div>
			</div>
			<!--main text-->
			<figure>
				<h4>Quel est le meilleur pour votre maison</h4>
				<figcaption>Armoires vs placards sans rendez-vous</figcaption>
			</figure>
			<!--anchor-->
		</li>

		<!-- === stretcher item === -->

		<li class="stretcher-item" style="background-image:url(<?php img_file('gallery-4.jpg'); ?>);">
			<!--logo-item-->
			<div class="stretcher-logo">
				<div class="text">
					<span class="f-icon f-icon-bathroom"></span>
					<span class="text-intro">Salle de bains</span>
				</div>
			</div>
			<!--main text-->
			<figure>
				<h4>Minimiser les choses</h4>
				<figcaption>Créer votre propre salle de bain</figcaption>
			</figure>
			<!--anchor-->
		</li>

		<!-- === stretcher item more=== -->

		<li class="stretcher-item more">
			<div class="more-icon">
				<span data-title-show="Voir plus" data-title-hide="+"></span>
			</div>
			<a href="<?php echo base_url() . 'produits'; ?>"></a>
		</li>

	</ul>
</section>
<!-- ========================  Blog Block ======================== -->

<section class="site-text text-center">
	<h2 class="h2 title">Le bon mobilier pour chaque pièce de votre maison</h2>
	<p><strong>Tunisie Meuble</strong> vous invite à découvrir sa gamme étendue de meubles pour la maison.</p>
	<ul class="display-inline">
		<li>Salons, canapés, living</li>
		<li>Salles à manger, argentières</li>
		<li>Chambre à coucher, armoires et gardes robes</li>
		<li>Chambre enfants, dressings sur mesure</li>
		<li>Meubles de cuisine et de salle de bain</li>
	</ul>
	<p> Tout ce dont vous avez besoin pour meubler et aménager votre intérieur :</p>
	<ul class="display-inline">
		<li>À votre goût</li>
		<li>Selon vos envies</li>
		<li>En respectant votre budget</li>
	</ul>
	<p>Nous proposons également des meubles d’entrée, des tables basses ou gigogne, des éléments porte-chaussure des bureaux et des meubles tv et accessoires d'ameublement de qualité et avec le style que vous aimez. </p>
</section>
<!-- ========================  Banner ======================== -->
<section class="banner" style="background-image:url(<?php img_file('gallery-4.jpg'); ?>)">
	<div class="container">
		<div class="row">
			<div class="col-md-offset-2 col-md-8 text-center">
				<h2 class="title">NOTRE HISTOIRE</h2>
				<p>
					Fondée en 2019, notre société a choisi de s’entourer d’une équipe d'experts expérimentés et spécialisés dans la vente, la production et la logistique de meubles. Nous exposons tous nos modèles dans notre showroom situé à La Soukra.
				</p>
				<p><a href="<?php echo base_url() . 'apropos'; ?>" class="btn btn-clean">Lire l'histoire complète</a></p>
			</div>
		</div>
	</div>
</section>
<!-- ========================  Blog ======================== -->
<section class="site-text text-center">
	<h2 class="h2 title">Des meubles de qualité au meilleur prix </h2>
	<p>Tous nos meubles sont fabriqués avec des <strong>matériaux de premier choix</strong> pour vous garantir un <strong>meilleur confort</strong> et une <strong>finition de haute qualité</strong></p>
	<p>Vous souhaitez meubler votre maison sans trop dépenser mais vous ne voulez pas renoncer au design et à la qualité? Vous êtes au bon endroit!</p>
	<p>Vous trouverez les <strong>meilleures offres de meubles à Tunis</strong>. Choisir des meubles en ligne et avoir la garantie de produits beaux et durables est désormais possible avec <strong><a target="_blank" href="https://goo.gl/maps/VmbkBexTDzWb3Cy98">Tunisie Meuble à la Soukra!</a></strong></p>
	<h2 class="h2 title">Commandez vos meubles sur mesure avec Tunisie Meuble à La Soukra</h2>
	<p>Nous créons des meubles sur mesure, personnalisables selon vos goûts et vos besoins. Nous profitons de chaque espace pour rendre votre maison confortable, unique et originale!</p>
	<p>Vous avez un modèle de canapé, de salon ou de chambre à coucher, un dressing sur mesure à reproduire ? Notre atelier de fabrication de meubles sur mesure est à votre disposition pour réaliser toutes vos envies !</p>
	<p>Pour profiter de nos services de création de meubles sur mesure, il suffit de nous décrire en détails vos besoins. Vous avez une photo, un modèle de meuble trouvé dans un catalogue en ligne ? Tunisie Meuble fabrique tous types de meubles sur commande. Faites confiance à notre expérience, demandez des informations au 24 005 160 ou au 25 602 380.</p>
</section>
<!-- ========================  Instagram ======================== -->
<section class="instagram">

	<!-- === instagram header === -->

	<header>
		<div class="container">
			<div class="row">
				<div class="col-md-offset-2 col-md-8 text-center">
					<h2 class="h2 title">SUIVEZ NOUS <i class="fa fa-instagram fa-2x"></i> Instagram </h2>
					<div class="text">
						<p>@tunisie-meuble</p>
					</div>
				</div>
			</div>
		</div>
	</header>

	<!-- === instagram gallery === -->

	<div class="gallery clearfix">
		<a class="item" href="javascript:void(0)">
			<img src="assets/images/square-1.jpg" alt="Alternate Text"/>
		</a>
		<a class="item" href="javascript:void(0)">
			<img src="assets/images/square-2.jpg" alt="Alternate Text"/>
		</a>
		<a class="item" href="javascript:void(0)">
			<img src="assets/images/square-3.jpg" alt="Alternate Text"/>
		</a>
		<a class="item" href="javascript:void(0)">
			<img src="assets/images/square-4.jpg" alt="Alternate Text"/>
		</a>
		<a class="item" href="javascript:void(0)">
			<img src="assets/images/square-5.jpg" alt="Alternate Text"/>
		</a>
		<a class="item" href="javascript:void(0)">
			<img src="assets/images/square-6.jpg" alt="Alternate Text"/>
		</a>

	</div> <!--/gallery-->

</section>
