<!-- ========================  Main header ======================== -->
<section class="main-header main-header-blog" style="background-image:url(<?php img_file('gallery-1.jpg'); ?>)">
	<header>
		<div class="container text-center">
			<h2 class="h2 title">À Propos de Tunisie Meuble</h2>
			<ol class="breadcrumb breadcrumb-inverted">
				<li><a href="<?php echo base_url(); ?>"><span class="icon icon-home"></span> Accueil</a></li>
				<li><a class="active" href="<?php echo base_url() . 'apropos' ?>">À Propos</a></li>
			</ol>
		</div>
	</header>
</section>
<section class="our-team">
	<div class="container">
		<!-- === Our team header === -->
		<div class="row">
			<div class="col-md-12 text-center">
				<h1 class="h2 title">A Propos de Tunisie Meuble </h1>
				<div class="text">
					<p><strong>Tunisie Meuble</strong> est l’une des entreprises leader de vente de meubles pour la maison. </p>
					<p>Fondée en 2019, notre société a choisi de s’entourer d’une équipe d'experts expérimentés et spécialisés dans la vente, la production et la logistique de meubles. Nous exposons tous nos modèles dans notre showroom situé à La Soukra.</p>
					<p>Forts d’une longue expérience dans la fabrication de meubles et de mobilier de bureau, nous avons affiné notre expertise dans la conception, la vente et l'assemblage de tous nos produits.</p>
					<p>Nous nous engageons à innover sans cesse et à concevoir des meubles au design moderne et classique pour satisfaire aux attentes de tous nos clients.</p>
					<p>Nous garantissons une qualité de conception et de finition hors-pair en prenant toujours  soin de vérifier que tous les meubles sortant de nos entrepôts soient conformes aux exigences  de qualité de nos clients </p>
					<p>Avec l'avènement d'Internet et sa consolidation en Tunisie, nous avons également décidé d'ouvrir notre <strong>propre boutique en ligne</strong> pour donner la possibilité à un public plus large de bénéficier de la possibilité de découvrir nos meubles  et nos solutions d’ameublement pour toutes les pièces de la maison.</p>
				</div>
				<div class="logo text-center">
					<img alt="Tunisie Meuble" title="Tunisie Meuble" width="250" src="<?php img_file('logo2.png'); ?>">
				</div>
			</div>
		</div>
	</div>
</section>
<!-- ================== Intro section default ================== -->
