<section class="main-header" style="background-image:url(<?php img_file('gallery-2.jpg'); ?>)">
	<header>
		<div class="container">
			<h1 class="h2 title">Sofa Laura</h1>
			<ol class="breadcrumb breadcrumb-inverted">
				<li><a href="<?php echo base_url(); ?>"><span class="icon icon-home"></span> Accueil</a></li>
				<li><a href="<?php if (isset($product)) {
						echo base_url() . 'produits/' . $product->category_permalink;
					} ?>"><?php echo $product->category_name; ?></a></li>
				<li><a href="javascript:void(0)"><?php echo $product->name; ?></a></li>
			</ol>
		</div>
	</header>
</section>
<section class="product">
	<div class="main">
		<div class="container">
			<div class="row product-flex">
				<!-- product flex is used only for mobile order -->
				<!-- on mobile 'product-flex-info' goes bellow gallery 'product-flex-gallery' -->
				<div class="col-md-4 col-sm-12 product-flex-info">
					<div class="clearfix">
						<!-- === product-title === -->
						<h1 class="title" data-title="<?php echo $product->name; ?>"><?php echo $product->name; ?></h1>
						<div class="clearfix">
							<!-- === price wrapper === -->
							<div class="price">
                                        <span class="h3">
                                           <?php echo $product->price; ?> TND
                                        </span>
							</div>
							<hr/>
							<div class="info-box">
								<span><strong>Catégorie</strong></span>
								<span><?php echo $product->category_name; ?></span>
							</div>
							<div class="info-box">
								<span><strong>Référence</strong></span>
								<span><?php echo 'RTM' . $product->id; ?></span>
							</div>
							<!-- === info-box === -->
							<div class="info-box">
								<span><strong>Prix</strong></span>
								<span><?php echo $product->price; ?> TND</span>
							</div>
							<!-- === info-box === -->
						</div> <!--/clearfix-->
					</div> <!--/product-info-wrapper-->
				</div> <!--/col-md-4-->
				<div class="col-md-8 col-sm-12 product-flex-gallery">
					<!-- === add to cart === -->
					<a href="<?php echo base_url() . 'commander/' . $product->id; ?>" class="btn btn-buy" data-text="+"></a>
					<div class="owl-product-gallery open-popup-gallery">
						<a href="<?php echo base_url() . $product->banner; ?>"><img
								src="<?php echo base_url() . $product->banner; ?>"
								alt="" height="500"/></a>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>
