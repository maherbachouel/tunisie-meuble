<!-- ======================== Main header ======================== -->

<section class="main-header" style="background-image:url(<?php img_file('gallery-4.jpg'); ?>)">
	<header>
		<div class="container text-center">
			<h1 class="h2 title">Produits</h1>
			<ol class="breadcrumb breadcrumb-inverted">
				<li><a href="<?php echo base_url(); ?>"><span class="icon icon-home"></span> Accueil</a></li>
				<li><a href="<?php echo base_url() . 'produits' ?>">Produits</a></li>
				<li><a href="javascript:void(0)"><?php if (!empty($currentCategory)) {
							echo $currentCategory->name;
						} ?></a></li>
			</ol>
		</div>
	</header>
</section>
<!-- ========================  Icons slider ======================== -->

<section class="owl-icons-wrapper">

	<!-- === header === -->
	<header class="hidden">
		<h2>Catégories de produits</h2>
	</header>
	<div class="container">
		<div class="owl-icons">
			<!-- === icon item === -->
			<?php if (!empty($category)) {
				foreach ($category as $cat) { ?>
					<a href="<?php echo base_url() . 'produits/' . $cat->permalink; ?>">
						<figure>
							<i class="<?php echo $cat->icon; ?>"></i>
							<figcaption><?php echo $cat->name; ?></figcaption>
						</figure>
					</a>
				<?php }
			} ?>
		</div> <!--/owl-icons-->
	</div> <!--/container-->
</section>
<!-- === product filters === -->

<section class="site-text text-center">
	<h2 class="h2 title">Un large choix de mobilier de cuisine et de salle à manger </h2>
	<p>Chez Tunisie Meuble, vous trouverez de nombreux meubles pour la salle à manger et des accessoires en compléments pour la cuisine:</p>
	<p>Consultez notre vaste catalogue et trouvez les meubles pour aménager votre espace nuit. Aménagez avec soin votre chambre double et les chambres de vos enfants pour faire de votre espace nuit un lieu idéal de détente et de douceur.</p>
	<p>Les chambres sont des pièces fondamentales, surtout parce qu'un bon repos et un bon lit sont les bons ingrédients pour bien commencer la journée. </p>
	<p>Nous adaptons tous nos modèles à vos envies et à vos besoins ! Pour avoir plus d’informations appelez nous au 24 005 160 ou au 25 602 380</p>
</section>
<section class="products">
	<div class="container">
		<header class="hidden">
			<h3 class="h3 title">Product category with topbar</h3>
		</header>
		<div class="row">
			<!-- === product-items === -->
			<?php if (isset($products)) {
				if (count($products)) {
					foreach ($products as $product) { ?>
						<div class="col-md-4 col-xs-6">
							<article>
								<div class="info">
								<span>
								  <a href="<?php echo base_url() . 'produits/' . $product->category_permalink . '/' . $product->permalink; ?>" data-title="Aperçu rapide"><i class="icon icon-eye"></i></a>
								</span>
								</div>
								<div class="btn btn-add">
									<a href="<?php echo base_url() . 'commander/' . $product->id; ?>"><i class="icon icon-cart"></i></a>
								</div>
								<div class="figure-grid">
									<span class="label label-info"><?php echo 'RTM'.$product->id; ?></span>
									<div class="image">
										<a href="<?php echo base_url() . 'produits/' . $product->category_permalink . '/' . $product->permalink; ?>">
											<img src="<?php echo base_url() . $product->banner; ?>" alt="" width="360"/>
										</a>
									</div>
									<div class="text">
										<h2 class="title h4"><a href="<?php echo base_url() . 'produits/' . $product->category_permalink . '/' . $product->permalink; ?>"><?php echo $product->name; ?></a></h2>
										<sup><?php echo $product->price; ?> TND</sup>
									</div>
								</div>
							</article>
						</div>
					<?php }
				} else { ?>
					<div class="alert alert-warning text-center" role="alert">
						Aucune produit dans cette catégorie
					</div>
				<?php }
			} ?>
		</div><!--/row-->
	</div><!--/container-->
</section>
<!-- ================== Footer  ================== -->
