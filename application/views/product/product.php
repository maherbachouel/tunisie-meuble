<!-- ======================== Main header ======================== -->

<section class="main-header" style="background-image:url(<?php img_file('gallery-4.jpg'); ?>)">
	<header>
		<div class="container text-center">
			<h1 class="h2 title">Produits</h1>
			<ol class="breadcrumb breadcrumb-inverted">
				<li><a href="<?php echo base_url(); ?>"><span class="icon icon-home"></span> Accueil</a></li>
				<li><a href="<?php echo base_url() . 'produits' ?>">Produits</a></li>
			</ol>
		</div>
	</header>
</section>
<!-- ========================  Icons slider ======================== -->

<section class="owl-icons-wrapper">

	<!-- === header === -->
	<header class="hidden">
		<h2>Product categories</h2>
	</header>
	<div class="container">
		<div class="owl-icons">
			<!-- === icon item === -->
			<?php if (!empty($category)) {
				foreach ($category as $cat) { ?>
					<a href="<?php echo base_url() . 'produits/' . $cat->permalink; ?>">
						<figure>
							<i class="<?php echo $cat->icon; ?>"></i>
							<figcaption><?php echo $cat->name; ?></figcaption>
						</figure>
					</a>
				<?php }
			} ?>
		</div> <!--/owl-icons-->
	</div> <!--/container-->
</section>
<!-- === product filters === -->
<section class="site-text text-center">
	<h2 class="h2 title">Tunisie Meuble, un grand choix de salons, canapés et séjours </h2>
	<p>Découvrez notre magasin de meubles dédié au mobilier de salon où vous pourrez trouver les meilleurs modèles de salons ou de séjours modernes ou classiques à prix réduits.</p>
	<p>Notre magasin de meubles de salon à la Soukra est constamment mis à jour. Nous proposons des collections de meubles de salons constamment mis au goût du jour mais également des modèles plus sobres et intemporels.</p>
</section>
<section class="products">

	<div class="container">

		<header class="hidden">
			<h3 class="h3 title">Product category with topbar</h3>
		</header>

		<div class="row">

			<!-- === product-items === -->

			<div class="col-md-12 col-xs-12">

				<div class="row">
					<!-- === product-item === -->
					<?php if (isset($products)) {
						if (count($products)) {
							foreach ($products as $product) { ?>
								<div class="col-md-4 col-sm-6 col-xs-12 " style="margin-bottom: 15px">
									<article>
										<div class="info">
							<span>
							  <a href="<?php echo base_url() . 'produits/' . $product->category_permalink . '/' . $product->permalink; ?>" data-title="Aperçu rapide"><i class="icon icon-eye"></i></a>
							</span>
										</div>
										<div class="btn btn-add">
											<a href="<?php echo base_url() . 'commander/' . $product->id; ?>"><i class="icon icon-cart"></i></a>
										</div>
										<div class="figure-grid">
											<span class="label label-info"><?php echo 'RTM' . $product->id; ?></span>
											<div class="image">
												<a href="<?php echo base_url() . 'produits/' . $product->category_permalink . '/' . $product->permalink; ?>">
													<img src="<?php echo base_url() . $product->banner; ?>" alt="" width="360"/>
												</a>
											</div>
											<div class="text">
												<h2 class="title h4"><a href="<?php echo base_url() . 'produits/' . $product->category_permalink . '/' . $product->permalink; ?>"><?php echo $product->name; ?></a></h2>
												<sup><?php echo $product->price; ?> TND</sup>
											</div>
										</div>
									</article>
								</div>
							<?php }
						} else { ?>
							<div class="alert alert-warning text-center" role="alert">
								Aucune produit trouvé
							</div>
						<?php }
					} ?>
				</div><!--/row-->
				<!--Pagination-->

			</div> <!--/product items-->
		</div><!--/row-->
		<!-- ========================  Product info popup - quick view ======================== -->
		<div class="popup-main mfp-hide" id="productid1">

			<!-- === product popup === -->

			<div class="product">

				<!-- === popup-title === -->

				<div class="popup-title">
					<div class="h1 title">Laura <small>product category</small></div>
				</div>

				<!-- === product gallery === -->

				<div class="owl-product-gallery">
					<img src="assets/images/product-1.png" alt="" width="640"/>
					<img src="assets/images/product-2.png" alt="" width="640"/>
					<img src="assets/images/product-3.png" alt="" width="640"/>
					<img src="assets/images/product-4.png" alt="" width="640"/>
				</div>

				<!-- === product-popup-info === -->

				<div class="popup-content">
					<div class="product-info-wrapper">
						<div class="row">

							<!-- === left-column === -->

							<div class="col-sm-6">
								<div class="info-box">
									<strong>Nom</strong>
									<span>Brand name</span>
								</div>
							</div>

							<!-- === right-column === -->

							<div class="col-sm-6">
								<div class="info-box">
									<strong>Couleurs disponibles</strong>
									<div class="product-colors clearfix">
										<span class="color-btn color-btn-red"></span>
										<span class="color-btn color-btn-blue checked"></span>
										<span class="color-btn color-btn-green"></span>
										<span class="color-btn color-btn-gray"></span>
										<span class="color-btn color-btn-biege"></span>
									</div>
								</div>
							</div>

						</div><!--/row-->
					</div> <!--/product-info-wrapper-->
				</div><!--/popup-content-->
				<!-- === product-popup-footer === -->

				<div class="popup-table">
					<div class="popup-cell">
						<div class="price">
							<span class="h3"><sup>590 TND</sup></small></span>
						</div>
					</div>
					<div class="popup-cell">
						<div class="popup-buttons">
							<a href="javascript:void(0);"><span class="icon icon-cart"></span> <span class="hidden-xs">Commander</span></a>
						</div>
					</div>
				</div>

			</div> <!--/product-->
		</div> <!--popup-main-->
	</div><!--/container-->
</section>
<!-- ================== Footer  ================== -->
