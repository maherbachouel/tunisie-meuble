<section class="main-header" style="background-image:url(<?php img_file('gallery-2.jpg'); ?>)">
	<header>
		<div class="container text-center">
			<h2 class="h2 title">Commander</h2>
		</div>
	</header>
</section>
<section class="checkout">
	<div class="container">
		<div class="cart-wrapper">
			<!--cart header -->
			<div class="cart-block cart-block-header clearfix">
				<div>
					<span>Produit</span>
				</div>
				<div>
					<span>&nbsp;</span>
				</div>
				<div>
					<span>Quantité</span>
				</div>
				<div class="text-right">
					<span>Prix</span>
				</div>
			</div>
			<!--cart items-->
			<div class="clearfix">
				<div class="cart-block cart-block-item clearfix">
					<div class="image">
						<a href="javascript:void(0)"><img src="<?php if (isset($product)) {
								echo base_url() . $product->banner;
							} ?>" title="<?php echo $product->name; ?>" alt="<?php echo $product->name; ?>"/></a>
					</div>
					<div class="title">
						<div class="h4"><?php echo $product->name; ?> (<?php echo 'RTM' . $product->id; ?>)</div>
					</div>
					<div class="quantity">
						<strong>1</strong>
					</div>
					<div class="price">
						<span class="final h3"><?php echo $product->price; ?> TND</span>
					</div>
				</div>
			</div>
			<div class="clearfix">
				<div class="cart-block cart-block-footer clearfix">
					<div>
						<strong>Totale à payer</strong>
					</div>
					<div>
						<div class="h2 title"> <?php echo $product->price; ?> TND</div>
					</div>
				</div>
			</div>
		</div>
		<!-- ========================  Delivery ======================== -->
		<div class="cart-wrapper">
			<div class="note-block">
				<div class="row">
					<!-- === left content === -->
					<form action="<?php echo base_url() . 'commander/' . $product->id; ?>" method="post">
						<div class="col-md-6">
							<!-- === login-wrapper === -->
							<div class="login-wrapper">
								<div class="white-block">
									<!--signin-->
									<div class="login-block">
										<div class="h4">Commander maintenant</div>
										<hr/>

										<div class="row">
											<div class="col-md-6">
												<div class="form-group">
													<input required type="text" name="firstname" class="form-control"
														   placeholder="Nom: *">
												</div>
											</div>
											<div class="col-md-6">
												<div class="form-group">
													<input required type="text" name="lastname" class="form-control"
														   placeholder="Prénom: *">
												</div>
											</div>
											<div class="col-md-12">
												<div class="form-group">
													<input required type="text" name="address" class="form-control"
														   placeholder="Address: *">
												</div>
											</div>
											<div class="col-md-6">
												<div class="form-group">
													<input required type="email" name="email" class="form-control"
														   placeholder="Email: *">
												</div>
											</div>
											<div class="col-md-6">
												<div class="form-group">
													<input required type="number" name="phone" class="form-control"
														   placeholder="Téléphone: *">
												</div>
											</div>
											<div class="col-md-12">
												<div class="form-group">
													<textarea style="resize: none;" name="comment" class="form-control"
															  placeholder="Commentaire:"></textarea>
												</div>
											</div>
										</div>
									</div> <!--/signup-->
								</div>
							</div> <!--/login-wrapper-->
						</div> <!--/col-md-6-->
						<!-- === right content === -->
						<div class="col-md-6">
							<div class="white-block">
								<div class="h4">Methode de paiement</div>
								<hr/>
								<span class="checkbox">
									<input type="radio" value="CASH" id="paymentID1" name="paymentOption"
										   checked="checked">
								<label for="paymentID1">
									<strong>Paiement en espèces à la livraison</strong> <br/>
								</label>
							</span>
								<span class="checkbox">
								<input type="radio" value="CHEQUE" id="paymentID2" name="paymentOption"
									   checked="checked">
								<label for="paymentID2">
									<strong>Paiement par chèque à la livraison</strong> <br/>
								</label>
							</span>
								<small>- Assurez-vous de préparer le montant exact de la commande. Les livreurs ne
									disposent pas toujours d'espèces pour vous rendre la monnaie.</small><br/>
								<small>- Le paiement par carte bancaire n'est pas accepté.</small>
							</div>
						</div>
						<div class="col-md-12">
							<button type="submit" class="btn btn-main btn-block">Commander
								maintenant
							</button>
						</div>
					</form>
				</div>
			</div>
		</div>
	</div>
	<!-- ========================  Cart wrapper ======================== -->
</section>
