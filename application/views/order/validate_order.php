<section class="not-found">
	<div class="container">
		<h1 class="title" ><span class="icon icon-checkmark-circle"></span></h1>
		<div class="h4 subtitle">Commande envoyée avec succès, nous vous rappelez le plus vite</div>
		<div class="h4 subtitle"> <a href="<?php echo base_url().'produits'; ?>">Nos Produits</a></div>
	</div>
</section>
