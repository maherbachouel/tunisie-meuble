<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Admin_Controller extends CI_Controller {
    public $current_user;


    public function __construct() {
        parent::__construct();
		$id = $this->session->userdata('uid');
		if ($id) {
			$this->load->model('user_model', 'account');
			$this->current_user = $this->account->GetUserById($id)[0];
			$this->role = $this->session->userdata('role');
		} else {
			redirect(base_url() . 'login');
		}
    }
}
